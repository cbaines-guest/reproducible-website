---
layout: report
year: "2020"
month: "11"
title: "Reproducible Builds in November 2020"
draft: false
date: 2020-12-10 14:48:32
---

[![]({{ "/images/reports/2020-11/reproducible-builds.png#right" | relative_url }})](https://reproducible-builds.org/)

**Greetings and welcome to the November 2020 report from the [Reproducible Builds](https://reproducible-builds.org) project.** In our monthly reports, we point out the most important things that have happened in and around our community.

---

* [![]({{ "/images/reports/2020-11/reploc-talk.png#right" | relative_url }})](https://kth.box.com/s/9t8itysrxdiq90ufnqno692k0ttz5zzc)

  Jifeng Xuan gave an online presentation titled [*Localization of Unreproducible Builds*](https://lists.reproducible-builds.org/pipermail/rb-general/2020-November/002077.html) to introduce a technique and tool called *RepLoc* that can identify the actual problematic, unreproducible files:

  > *RepLoc* features a query augmentation component that utilizes the information extracted from the build logs and a heuristic rule-based filtering component that narrows the search scope. By integrating the two components with a weighted file ranking module, *RepLoc* is able to automatically produce a ranked list of files that are helpful in locating the problematic files for the unreproducible builds.

  A [recording of Xuan's talk is available](https://kth.box.com/s/9t8itysrxdiq90ufnqno692k0ttz5zzc), as is a [PDF of the associated academic article](https://arxiv.org/pdf/1803.06766.pdf) which was co-written by Zhilei Ren, He Jiang and Zijiang Yan and Zijiang Yang.

* [![]({{ "/images/reports/2020-11/precursor.png#right" | relative_url }})](https://www.crowdsupply.com/sutajio-kosagi/precursor)

  The [Precursor](https://www.crowdsupply.com/sutajio-kosagi/precursor) project aims to make a complete hardware and software solution for secure and private communications. It is based on the RISC-V platform, and at the time of writing it is [close to reaching its crowdfunding target](https://www.crowdsupply.com/sutajio-kosagi/precursor). This month, a [post on Andrew "bunnie" Huang's blog](https://www.bunniestudios.com/blog/?p=5971) describes more about the technical details of the project, highlighting that its builds are entirely reproducible.

* [*diffware*](https://github.com/airbus-seclab/diffware/) is a new [*diffoscope*](https://diffoscope.org)-like tool that provides a summary of changes between two files or directories. It can be configured to retain only the changes that matter to the user, and can actually be combined with *diffoscope* itself to dive deeper into differences it finds.

* [![]({{ "/images/reports/2020-11/cwa.png#right" | relative_url }})](https://github.com/corona-warn-app/cwa-app-android)

  The [Corona Warn App](https://github.com/corona-warn-app/cwa-app-android) is a fork of the German Corona App, where data is stored locally on each user's device, preventing authorities or other parties from accessing and controlling the data. It doesn't use the Google-provided services, yet it remains compatible with the official app. It will shortly be available on the [F-Droid](https://f-droid.org/) free-software app store, and it is has been reported that, once available, it [will be bit-for-bit reproducible](https://github.com/corona-warn-app/cwa-app-android/issues/1483#issuecomment-734491614). (German press coverage: [Heise.de](https://www.heise.de/news/Corona-Warn-App-in-Kuerze-bei-F-Droid-verfuegbar-4973701.html) & [Golem.de](https://www.golem.de/news/microg-f-droid-arbeitet-an-freiem-paket-der-corona-warn-app-2011-152463.html))

* The [*rebuilderd*](https://github.com/kpcyrd/rebuilderd) project released three new versions this month, adding support for [*diffoscope*](https://diffoscope.org]), better build log handling and dramatically improving the prioritisation of new and failed builds. *rebuilderd* has been powering [Arch Linux's reproducible efforts](https://reproducible.archlinux.org/) since April 2020 where it has been used to determine that approximately 80% of [Arch Linux](https://archlinux.org)'s packages are reproducible.


## Distribution work

[![]({{ "/images/reports/2020-11/yoctoproject.png#right" | relative_url }})](https://www.yoctoproject.org/)

The [Yocto Project](https://www.yoctoproject.org/) has been quietly working on improving reproducibility. As [reported in January 2020]({{ "/reports/2020-11/" | relative_url }}), its `core-image-minimal` target packages are bit-for-bit reproducible regardless of the build system's distribution or the directory used to perform the build. Starting with the first milestone release in the current development cycle, the entire `world` packages target for all 11,271 packages in `OpenEmbedded-Core` are now reproducible, with the exception of 65 packages. New targets will be added to the existing automated testing to ensure regressions can be spotted quickly.

[![]({{ "/images/reports/2020-11/debian.png#right" | relative_url }})](https://debian.org/)

In recent months there has been preparatory work to enable the [`reproducible=+fixfilepath` build flag](https://alioth-lists.debian.net/pipermail/reproducible-builds/Week-of-Mon-20200921/012586.html) by default. Enabling this `fixfilepath` feature flag will fix reproducibility issues in an estimated 500-700 packages. After [previous discussion](https://lists.debian.org/debian-devel/2020/10/msg00222.html) a discussion on the [debian-devel](https://lists.debian.org/debian-devel/) mailing list, Vagrant Cascadian [filed a bug](https://bugs.debian.org/974087) to explicitly propose a patch for the `dpkg` developers.

Vagrant Cascadian also [disabled parallel builds](https://salsa.debian.org/debian/guix/-/commit/5aa250d0ed87d42a8bccf08bae50bf8ea88a9332) in Debian's `guix` package in order to fix [a number of reproducibility issues](https://issues.guix.gnu.org/20272), filing a [separate upstream bug report](https://issues.guix.gnu.org/44835) pertaining to embedded build paths. Vagrant additionally made [non-maintainer uploads](https://wiki.debian.org/NonMaintainerUpload) of the `texi2html`&nbsp;[[...](https://tracker.debian.org/news/1192796/accepted-texi2html-182dfsg1-6-source-into-unstable/)] and `intltool`&nbsp;[[...](https://tracker.debian.org/news/1197147/accepted-intltool-0510-51-source-into-unstable/)] packages to Debian in order to fix two toolchain issues.

We also added to our [knowledge about identified issues](https://tests.reproducible-builds.org/debian/index_issues.html), as 171 reviews of Debian packages were added, 22 were updated and 25 were removed this month. As part of this, Chris Lamb identified and categorised three new toolchain issues: [`build_path_captured_by_pyuic5`](https://salsa.debian.org/reproducible-builds/reproducible-notes/commit/c60613fc), [`build_path_captured_by_octave`](https://salsa.debian.org/reproducible-builds/reproducible-notes/commit/f406672a) & [`build_path_captured_by_nim`](https://salsa.debian.org/reproducible-builds/reproducible-notes/commit/de4a15e0).

[![]({{ "/images/reports/2020-11/opensuse.png#right" | relative_url }})](https://www.opensuse.org/)

In the [openSUSE](https://www.opensuse.org/) distribution, Bernhard M. Wiedemann published his [monthly Reproducible Builds status update](https://lists.opensuse.org/archives/list/factory@lists.opensuse.org/message/2ZZQJOZ2CYKLL2AVC7NBFRDWB57K5JKU/).

## Upstream patches

The following patches were created this month:

* Arnout Engelen:

    * [`dbus`](https://gitlab.freedesktop.org/dbus/dbus/-/merge_requests/189) ([DocBook](http://docbook.sourceforge.net/) configuration missing [`generate.consistent.ids`](http://docbook.sourceforge.net/release/xsl/current/doc/html/generate.consistent.ids.html) option).

* Bernhard M. Wiedemann:

    * [`herbstluftwm`](https://github.com/herbstluftwm/herbstluftwm/pull/1029) ([CMake](https://cmake.org/)-related date)
    * [`herbstluftwm`](https://github.com/herbstluftwm/herbstluftwm/pull/1030) (filesystem ordering)
    * [`OpenRGB`](https://build.opensuse.org/request/show/849249) (date issue, [already upstream](https://github.com/CalcProgrammer1/OpenRGB/commit/f1b7b8ba900db58a1119d8d3e21c1c79de5666aa))
    * [`procmail`](https://build.opensuse.org/request/show/850841) (captures groups from the surrounding build environment)
    * [`x3270`](https://build.opensuse.org/request/show/847053) (date, [already upstream](https://sourceforge.net/p/x3270/code/merge-requests/2/))

* Chris Lamb:

    * [#973595](https://bugs.debian.org/973595) filed against [`emscripten`](https://tracker.debian.org/pkg/emscripten) (randomness in output, [forwarded upstream](https://github.com/emscripten-core/emscripten/pull/12679)).
    * [#973601](https://bugs.debian.org/973601) filed against [`sympow`](https://tracker.debian.org/pkg/sympow) (captures build path).
    * [#973801](https://bugs.debian.org/973801) filed against [`python-pairix`](https://tracker.debian.org/pkg/python-pairix) (captures build [umask](https://en.wikipedia.org/wiki/Umask)).
    * [#973806](https://bugs.debian.org/973806) filed against [`metakernel`](https://tracker.debian.org/pkg/metakernel) (captures build year in documentation).
    * [#973964](https://bugs.debian.org/973964) filed against [`python-biom-format`](https://tracker.debian.org/pkg/python-biom-format) (non-deterministic `.coverage` file).
    * [#974124](https://bugs.debian.org/974124) filed against [`less.js`](https://tracker.debian.org/pkg/less.js) (timestamps in copyright headers).
    * [#974573](https://bugs.debian.org/974573) filed against [`os-autoinst`](https://tracker.debian.org/pkg/os-autoinst) (ships non-deterministic `.packlist` file).
    * [#974904](https://bugs.debian.org/974904) filed against [`armagetronad`](https://tracker.debian.org/pkg/armagetronad) (timestamps in generated version number).
    * [#975046](https://bugs.debian.org/975046) filed against [`open-iscsi`](https://tracker.debian.org/pkg/open-iscsi) (timestamps in generated documentation).
    * [#975954](https://bugs.debian.org/975954) filed against [`amavisd-milter`](https://tracker.debian.org/pkg/amavisd-milter) (`PACKAGE_VERSION` based on current date).
    * [#975958](https://bugs.debian.org/975958) filed against [`requirejs`](https://tracker.debian.org/pkg/requirejs) (timestamps in build configuration).

* *kpcyrd*:

    * [`plasma-framework`](https://invent.kde.org/frameworks/plasma-framework/-/merge_requests/139) (embedded build timestamps in Gzip metadata).
    * [`signal-desktop`](https://github.com/snyk/snyk/pull/1537) (embedded build timestamp).

* Vagrant Cascadian:

    * [#974087](https://bugs.debian.org/974087) filed against [`dpkg`](https://tracker.debian.org/pkg/dpkg).
    * [#974863](https://bugs.debian.org/974863) filed against [`vboot-utils`](https://tracker.debian.org/pkg/vboot-utils).
    * [#974911](https://bugs.debian.org/974911) filed against [`debian-policy`](https://tracker.debian.org/pkg/debian-policy).
    * [#974942](https://bugs.debian.org/974942) filed against [`git`](https://tracker.debian.org/pkg/git).
    * [#974957](https://bugs.debian.org/974957) filed against [`dynare`](https://tracker.debian.org/pkg/dynare).
    * [#974959](https://bugs.debian.org/974959) filed against [`dynare`](https://tracker.debian.org/pkg/dynare).
    * [#974960](https://bugs.debian.org/974960) filed against [`libforms`](https://tracker.debian.org/pkg/libforms).
    * [#975025](https://bugs.debian.org/975025) filed against [`flex`](https://tracker.debian.org/pkg/flex).
    * [#975373](https://bugs.debian.org/975373) filed against [`sugar-read-activity`](https://tracker.debian.org/pkg/sugar-read-activity).
    * [#975374](https://bugs.debian.org/975374) filed against [`sugar-calculate-activity`](https://tracker.debian.org/pkg/sugar-calculate-activity).
    * [#975504](https://bugs.debian.org/975504) filed against [`obs-studio`](https://tracker.debian.org/pkg/obs-studio).
    * [#976071](https://bugs.debian.org/976071) filed against [`xtpcpp`](https://tracker.debian.org/pkg/xtpcpp).

## Tools

[![]({{ "/images/reports/2020-11/diffoscope.svg#right" | relative_url }})](https://diffoscope.org)

[*diffoscope*](https://diffoscope.org) is the Reproducible Build's project in-depth and content-aware diff utility. Not only can it locate and diagnose reproducibility issues, it provides human-readable diffs from many kinds of binary format.

This month, Chris Lamb [uploaded version `162` to Debian](https://tracker.debian.org/news/1197106/accepted-diffoscope-162-source-all-into-unstable-unstable/) (later [backported by Mattia Rizzolo](https://tracker.debian.org/news/1187659/accepted-diffoscope-161bpo101-source-into-buster-backports/)), as well as made the following changes:

* Improvements:

    * Move the slightly-confusing behaviour if a single file is passed to *diffoscope* on the command-line to a new `--load-existing-diff` command.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/0d851ae)]
    * Ensure the new `diffoscope-minimal` package that was introduced by Mattia Rizzolo has a different short description from the primary `diffoscope` one.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/716efb1)]
    * Refresh the long and short descriptions of all of the Debian packages.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/7fae8af)]

* Bug fixes:

    * Don't depend on [radare2](https://rada.re/n/) in the Debian 'autopkgtests' as it will not be in *bullseye* due to security considerations. ([#975313](https://bugs.debian.org/975313))
    * Avoid some incorrectly-formatted error messages. This was caused by *diffoscope* raising an artificial `CalledProcessError` exception in a generic handler.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/1fecab9)]

* Codebase improvements:

    * Add a comment regarding Java tests to help *diffoscope* contributors who are not developing using Debian&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/45d7567)] and don't use the old-style `super(...)` call&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/d774de4)].

In addition, Conrad Ratschan added a comparator for "legacy" [uboot](https://en.wikipedia.org/wiki/Das_U-Boot) uImage files to *diffoscope* ([!69](https://salsa.debian.org/reproducible-builds/diffoscope/-/merge_requests/69)), Mattia Rizzolo split the `diffoscope` package into a `diffoscope-minimal` package which excludes the larger packages from its `Recommends` ([#975261](https://bugs.debian.org/975261)) and Jelmer Vernooĳ added a missing space to an error message&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/b87e3b2)].

Elsewhere in our tooling, Holger Levsen also bumped the `Standards-Version` headers in strip-nondeterminism&nbsp;[[...](https://salsa.debian.org/reproducible-builds/strip-nondeterminism/commit/306339f)], diffoscope&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/0464e5a)], disorderfs&nbsp;[[...](https://salsa.debian.org/reproducible-builds/disorderfs/commit/17c4dc9)] and reprotest&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reprotest/commit/8962784)], as well as updated the `tox.ini` test configuration for `reprotest` and filed a bug after noticing that its testsuite is not run during the build ([#975094](https://bugs.debian.org/975094))

## Testing framework

[![]({{ "/images/reports/2020-11/testframework.png#right" | relative_url }})](https://tests.reproducible-builds.org/)

The Reproducible Builds project operates a large [Jenkins](https://jenkins.io/)-based testing framework that powers [`tests.reproducible-builds.org`](https://tests.reproducible-builds.org). This month, Holger Levsen made the following changes:

* [Debian](https://debian.org/)-related changes:

    * Stop testing the Debian *buster* distribution, except for new package versions.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/c1da4e8c)]
    * Fix a typo when setting up logs to run *diffoscope*.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/a29786c6)]
    * Delete old *bullseye* and *unstable* build environments even sooner.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/8e0c6058)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/79ce39e6)]
    * Detect failures to update Debian's "`chdist`".&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/37f6e918)]

* Node provisioning scripts:

    * Add debug output if `daemon-reload` calls fails, etc.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/9398b25d)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/d814ee70)]
    * Drop the `bring_back_node.sh` script; using `vim` is simpler here.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/7a473a18)]
    * Improve documentation of `builtin-pho` database setup.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/5b95f499)]
    * Add more fine-tuned colour indication of filesystem usage on the Jenkins shell monitor.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/f864b860)]

* Other distributions:

    * Ensure that that [FreeBSD](https://www.freebsd.org/) test virtual machines are upgraded to version 12.2.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/69a738d0)]
    * Enable building of all [OpenWrt](https://openwrt.org/) packages again.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/faa25812)]
    * Detect failure to update [Arch Linux](https://www.archlinux.org/) build environments.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/45492789)]

* System health checks & notifications:

    * Detect [`etckeeper`](https://etckeeper.branchable.com/) system service failures.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/902a685a)]
    * Update diskspace warnings.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/dc5bc940)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/12edac2d)]
    * Provide empty placeholders for machines going down.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/cb43b9fd)]
    * Don't alert if the version of *diffoscope* in Debian is behind [PyPi](https://pypi.org/).&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/331d1ebc)]
    * Move some IRC notifications to `#reproducible-changes`.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/37b3d5f6)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/9c81df05)]
    * Suppress noise when showing offline nodes in the Jenkins shell monitor.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/3d2ceb6c)]

* Documentation:

    * Document the [server status page](https://tests.reproducible-builds.org/trbo.status.html).&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/2d83ff61)]
    * Update a 'FIXME' regarding the [Jenkins' remoting CLI](https://www.jenkins.io/blog/2019/02/17/remoting-cli-removed/), as there's nothing we can do.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/2379525f)]
    * Move documentation about [OSUOSL](https://osuosl.org/)-hosted nodes to the right place.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/d2f3349f)]
    * Document how to run the `jenkins-shell-monitor.sh`.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/506979f8)]

Build node maintenance was also performed by Holger Levsen&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/2d90b19c)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/1229af1d)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/b7944604)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/b765ed0f)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/d491ba0e)], Mattia Rizzolo&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/21ed7d13)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/78f4ad0d)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/b5bb7434)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/f193491d)] and Vagrant Cascadian&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/a63fc75a)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/0f57626b)].

## Community changes

Chris Lamb updated the [main Reproducible Builds website and documentation](https://reproducible-builds.org/) to clarify that the [`SOURCE_DATE_EPOCH`](https://reproducible-builds.org/specs/source-date-epoch) environment variable is not Debian specific&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/84154a0)], and made a number of miscellaneous cosmetic changes&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/d382556)][[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/8c23447)].

There was significant IRC activity during November too. Not only did we create a new IRC channel to capture notifications&nbsp;[[...](https://lists.reproducible-builds.org/pipermail/rb-general/2020-November/002089.html)], we also hosted a total four meetings: the first were on general topics&nbsp;[[...](http://meetbot.debian.net/reproducible-builds/2020/reproducible-builds.2020-11-09-18.08.html)][[...](http://meetbot.debian.net/reproducible-builds/2020/reproducible-builds.2020-11-23-17.58.html)] as well as specific session on [how to debug various distributions](http://meetbot.debian.net/reproducible-builds/2020/reproducible-builds.2020-11-16-18.08.html). We then held our first 'Ask Me Anything' (AMA) as an opportunity for people to ask introductory questions&nbsp;[[...](http://meetbot.debian.net/reproducible-builds/2020/reproducible-builds.2020-11-30-17.19.html)]. Another AMA session will be held on **7th January 2021**.

---

If you are interested in contributing to the Reproducible Builds project, please visit our [*Contribute*](https://reproducible-builds.org/contribute/) page on our website. However, you can get in touch with us via:

 * IRC: `#reproducible-builds` on `irc.oftc.net`.

 * Twitter: [@ReproBuilds](https://twitter.com/ReproBuilds)

 * Mastodon: [@reproducible_builds@fosstodon.org](https://fosstodon.org/@reproducible_builds)

 * Reddit: [/r/ReproducibleBuilds](https://reddit.com/r/reproduciblebuilds)

 * Mailing list: [`rb-general@lists.reproducible-builds.org`](https://lists.reproducible-builds.org/listinfo/rb-general)
