---
layout: report
year: "2021"
month: "01"
title: "Reproducible Builds in January 2021"
draft: false
date: 2021-02-04 14:54:34
---

[![]({{ "/images/reports/2021-01/reproducible-builds.png#right" | relative_url }})](https://reproducible-builds.org/)

**Welcome to the report from the [Reproducible Builds](https://reproducible-builds.org) project for January 2021.** In our reports we outline the most important things that have happened in the world of reproducible builds in the past month. If you are interested in contributing to the project, please visit our [*Contribute*]({{ "/contribute/" | relative_url }}) page on our website.

<br>

[![]({{ "/images/reports/2020-12/solarwinds.jpg#right" | relative_url }})](https://www.washingtonpost.com/technology/2020/12/14/russia-hack-us-government/)

There has been further discussion in security circles around the recent 'SolarWinds' supply-chain attack ([covered in our report last month]({{ "/reports/2020-12/" | relative_url }}). This month, however, David A. Wheeler posted an article on the [Linux Foundation's blog](https://www.linuxfoundation.org/en/resources/blog/) titled [*Preventing Supply Chain Attacks like SolarWinds*](https://www.linuxfoundation.org/en/blog/preventing-supply-chain-attacks-like-solarwinds/).

Noting that "assuming a system can never be broken into is a failing strategy", David continues:

> In the longer term, I know of only one strong countermeasure for this kind of attack: **verified reproducible builds**. A “reproducible build” is a build that always produces the same outputs given the same inputs so that the build results can be verified. A verified reproducible build is a process where independent organizations produce a build from source code and verify that the built results come from the claimed source code. Almost all software today is not reproducible, but there’s work to change this.

In addition, [Episode 101](https://ubuntusecuritypodcast.org/episode-101/) of the [Ubuntu Security Podcast](https://ubuntusecuritypodcast.org) also covered the SolarWinds hack in further detail.

<br>

[![]({{ "/images/reports/2021-01/bootstrappable-builds.png#right" | relative_url }})](https://lwn.net/Articles/841797/)

Elsewhere, the [Bootstrappable Builds](https://bootstrappable.org/) project was covered in depth by Jake Edge on [Linux Weekly News](https://lwn.net/). Jake introduced this sister project as follows:

> The Bootstrappable Builds project was started as an offshoot of the Reproducible Builds project during the latter's 2016 [summit in Berlin]({{ "/events/berlin2016/" | relative_url }}). A *bootstrappable build* takes the idea of reproducibility one step further, in some sense. The build of a target binary can be reproduced alongside the build of the tools required to do so. It is, conceptually, almost like building a house from a large collection of atoms of different elements.
>
> [...]
>
> Building software depends on the tools used to construct the binary, including compilers and build-automation tools, many of which depend on pre-existing binaries. Minimizing the reliance on opaque binaries for building our software ecosystem is the goal of the Bootstrappable Builds project.

The [full article is available](https://lwn.net/Articles/841797/) on the LWN website.

<br>

[![]({{ "/images/reports/2021-01/outreachy.png#right" | relative_url }})](https://lists.reproducible-builds.org/pipermail/rb-general/2021-January/002166.html)

[Outreachy](https://www.outreachy.org/) is an initiative that funds three-month remote internships in free and open source software, with a focus and background on supporting diversity. The Reproducible Builds project is considering joining this round, and are seeking input and ideas for good proposals.

Examples of the kind of projects we are looking for include workflow changes, large refactoring work, new features of our tools, specific reproducibility fixes and so on. Ideas should fit in that sweet spot of requiring more time and energy than a weekend project, but are also not too complicated that they would take forever. For more information, please see [Mattia's announcement on our mailing list](https://lists.reproducible-builds.org/pipermail/rb-general/2021-January/002166.html).

## Software development

### Debian

[![]({{ "/images/reports/2021-01/debian.png#right" | relative_url }})](https://debian.org/)

In recent months there has been preparatory work to enable the [`reproducible=+fixfilepath` build flag](https://alioth-lists.debian.net/pipermail/reproducible-builds/Week-of-Mon-20200921/012586.html) by default; enabling this `fixfilepath` feature flag should fix reproducibility issues in an estimated 500-700 packages. In January, however, Guillem Jover uploaded [`dpkg` version 1.20.6 to Debian *unstable*](https://bugs.debian.org/974087#37) with this flag enabled. Although a bug ([#979570](https://bugs.debian.org/979570)) was subsequently filed by Lisandro Damián Nicanor Pérez Meyer with the initial intention of pausing this change due to a problem with the [Qt toolkit](https://www.qt.io/), [it was closed](https://bugs.debian.org/979570#40) after extensive discussion.

In recent weeks, Holger Levsen has been re-uploading a large number of Debian packages in an attempt to ensure they all have a related `.buildinfo` file. Holger described his rationale and approach in a blog post in December titled [*On doing 540 no-source-change source-only uploads in two weeks*](http://layer-acht.org/thinking/blog/20201231-no-source-change-source-uploads/). In January, however, Holger performed 2,940 of these uploads, resulting in the Debian *bullseye* being brought down down to *eleven* packages that lack these files (from over 3,500). Holger [wrote about his progress](https://lists.debian.org/debian-release/2021/01/msg00415.html) on our mailing list, where he also describes how he intends to eliminate the remaining packages.

[![]({{ "/images/reports/2021-01/intoto.png#right" | relative_url }})](https://in-toto.io/)

Lukas Puehringer, Frédéric Pierre and Holger Levsen collaborated to upload [`apt-transport-in-toto`](https://github.com/in-toto/apt-transport-in-toto) version 0.1.0 into the `unstable` distribution, and Lukas Puehringer prepared packages for [`in-toto`](https://in-toto.io/) version 1.0.0 and [python-securesystemslib](https://github.com/secure-systems-lab/securesystemslib) 0.18.0 to the *unstable* distribution.

35 reviews of Debian packages were added, 58 were updated and 49 were removed this month adding to [our extensive knowledge about identified issues](https://tests.reproducible-builds.org/debian/index_issues.html). Chris Lamb identified two issue categories, [`build_path_added_by_src2man_from_txt2man`](https://salsa.debian.org/reproducible-builds/reproducible-notes/commit/1d7c9645) and [`nondeterminstic_todo_identifiers_in_documentation_generated_by_doxygen`](https://salsa.debian.org/reproducible-builds/reproducible-notes/commit/3ff8730e). Thorsten Glaser also added a new `uid_and_gid_in_cmake-generated_pkzip` issue type as well&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reproducible-notes/commit/ed9f3864)].

### Other distributions

[![]({{ "/images/reports/2021-01/opensuse.png#right" | relative_url }})](https://www.opensuse.org/)

Bernhard M. Wiedemann posted his [monthly reproducible builds status report](https://lists.opensuse.org/archives/list/factory@lists.opensuse.org/thread/VDQKQ2CTOBGMYJMCA6QR7WDMMFBXWXB7/) for openSUSE which mentions amongst other things that "4.10% of packages are not perfectly reproducible".

[Jelle van der Waa](https://vdwaa.nl/) posted an overview of Arch Linux's work on reproducible builds during 2020. Titled [*Arch Linux Reproducible Builds Progress 2020*](https://vdwaa.nl/arch-linux-reproducible-builds-progress-2020.html), it mentions (for example) that their [*rebuilderd*](https://github.com/kpcyrd/rebuilderd) tool has seen 13 releases since March 2020.

### [*reprotest*](https://tracker.debian.org/pkg/reprotest)

[*reprotest*](https://tracker.debian.org/pkg/reprotest) is our end-user tool to build same source code twice in widely differing environments and then checks the binaries produced by each build for any differences. This month, the following changes were made:

* Frédéric Pierret:

    * Add an RPM spec file.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reprotest/commit/6c00d04)][[...](https://salsa.debian.org/reproducible-builds/reprotest/commit/07bd29f)][[...](https://salsa.debian.org/reproducible-builds/reprotest/commit/3a28437)]
    * Improve the tests.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reprotest/commit/7632e27)][[...](https://salsa.debian.org/reproducible-builds/reprotest/commit/f0dda95)]
    * Fix a number of deprecation warnings.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reprotest/commit/1a29a1b)]
    * Update `.gitignore`.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reprotest/commit/a8a7f24)]
    * Replace deprecated `warn` method in logging routines.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reprotest/commit/9f86fbb)]
    * Improve documentation on available verbosity values.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reprotest/commit/4751630)]
    * Update `README` documentation for RPM support.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reprotest/commit/40899e2)]

* Holger Levsen:

    * Upload to Debian *unstable*.&nbsp;[[...](https://tracker.debian.org/news/1222323/accepted-reprotest-0716-source-into-unstable/)]

* Marek Marczykowski-Górecki:

    * When running continuous integration, don't run *reprotest* on *reprotest* itself.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reprotest/commit/cfcd5d6)]
    * Disable running tests on Python 3.8.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reprotest/commit/0aa3ae6)]

#### [*diffoscope*](https://diffoscope.org)

[![]({{ "/images/reports/2021-01/diffoscope.svg#right" | relative_url }})](https://diffoscope.org)

[*diffoscope*](https://diffoscope.org) is our project in-depth and content-aware diff utility. Not only can it locate and diagnose reproducibility issues, it provides human-readable diffs from many kinds of binary format. This month, Chris Lamb made a large number of changes (including releasing [version 164](https://diffoscope.org/news/diffoscope-163-released/), [version 165](https://diffoscope.org/news/diffoscope-163-released/) and [version 166](https://diffoscope.org/news/diffoscope-163-released/)):

* New features:

    - Save `sys.argv` in our top-level temporary directory, in case it helps debug why temporary directories might not get cleaned up.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/e005735)]
    - Collapse the `--acl` and `--xattr` arguments into `--extended-filesystem-attributes` to cover all of these extended attributes, defaulting the new option to false (ie. to not check these expensive external calls).&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/8317da3)][[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/e5f598b)]

* Bug fixes:

    - Explicitly remove our top-level temporary directory. ([#981123](https://bugs.debian.org/981123))
    - Adjust the fuzzy matching threshold to ensure that we show more differences.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/8c50cb8)][[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/c0a19a2)]
    - Use `magic.Magic()` over the now-deprecated `magic.open()` compatibility interface.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/c72c30f)]

* Output improvements:

    - Show the 'fuzziness' amount in percentage terms, not out of the rather-arbitrary '400'.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/42ebb62)]
    - Improve help text for the `--exclude-directory-metadata` argument.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/959d423)]
    - Wrap our external call to `cmp(1)` with a missing profiling point.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/a1cc1c0)]
    - Truncate `jsondiff` differences at 512 bytes, in case they consume the entire page.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/56d20d0)]
    - Improve the logging around fuzzy matching.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/005b274)]

* Codebase improvements:

    - Clarify in a comment that `__del__` is not always called in Python, so temporary directories are not necessarily removed the *moment* they go out of scope.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/ee581fd6)]
    - Print the free space in our temporary directory when we create it, not from within `diffoscope.main`.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/8a87284)]
    - Tidy the `diffoscope.comparators.utils.fuzzy` module.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/62ae0bb)]
    - Add a note regarding the special ordering of `test_all_tools_are_listed` within that module.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/9a68dec)]

Other changes were made by:

* Conrad Ratschan:

    * Add a comparator for [U-Boot](https://www.denx.de/wiki/U-Boot/) Flattened Image Tree (FIT) files.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/ebd3bdc)][[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/74ca80d)]

* Dimitrios Apostolou:

    * Introduce the `--no-acl` and `--no-xattr arguments` (later collapsed to `--extended-filesystem-attributes` by Chris Lamb) to improve performance.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/9e3abf9)]
    * Avoid calling the external stat command.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/f7d8be3)]
    * Avoid invoking external `diff` command for short outputs that are identical.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/223b158)]
    * Log when the `cmp` command is spawned.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/cc38d8e)]
    * Improve performance of the `has_same_content` routine by spawning `cmp` less frequently.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/80bf1ae)]
    * Cleanup the FIFO files when our context manager exits.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/f681cfb)]

* Mattia Rizzolo:

    * Add missing `lipo` and `otool` external tools, and add a test to make sure they are all listed.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/72fe534)]
    * Fix a possible crash in the `--list-debian-substvars` command.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/c982024)]
    * Filter the content of the `debian/*.substvars` files.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/201d250)]
    * Ignore/hide the `DeprecationWarning` pertaining to the `imp` module deprecation as it comes from a 3rd-party library.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/b359478)]
    * Add a `pytest.ini` to explicitly generate [JUnit](https://junit.org/junit5/)'s *xunit2* format.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/5cdac59)]
    * Override several [Lintian](https://lintian.debian.org) warnings regarding prebuilt test binaries existing in the source tree.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/bbcd902)]

### Other tools

[*strip-nondeterminism*](https://tracker.debian.org/pkg/strip-nondeterminism) is our tool to remove specific non-deterministic results from a completed build. This month, Chris Lamb ensured that the tool did not process unwritable files (printing a warning in this case) ([#980356](https://bugs.debian.org/980356)) as well as a number of codebase improvements including reflowing logic to make larger future changes easier.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/strip-nondeterminism/commit/0c41f07)]

[*disorderfs*](https://tracker.debian.org/pkg/disorderfs) is our [FUSE](https://en.wikipedia.org/wiki/Filesystem_in_Userspace)-based filesystem that deliberately introduces non-determinism into system calls to reliably flush out reproducibility issues. This month, Chris Lamb updated the benchmarking tools to call a tool that will call `stat(2)` repeatedly&nbsp;[[...](https://salsa.debian.org/reproducible-builds/disorderfs/commit/056270e)] and Frédéric Pierret added an RPM spec file&nbsp;[[...](https://salsa.debian.org/reproducible-builds/disorderfs/commit/ad63c99)] as well as the ability to prepend flags in `CXXFLAGS`&nbsp;[[...](https://salsa.debian.org/reproducible-builds/disorderfs/commit/20c4ea0)]. Holger Levsen uploaded these changes to Debian *unstable* as version `0.5.11-1`. *disorderfs* was also [featured on Hacker News](https://news.ycombinator.com/item?id=25942451).

### Upstream patches

The Reproducible Builds project detects, dissects and attempts to fix as many currently-unreproducible packages as possible. We endeavour to send all of our patches upstream where appropriate. This month, we wrote a large number of such patches, including:

* Arjen de Korte [created a pull request](https://github.com/php/php-src/pull/6564) for the [PHP programming language](https://www.php.net) to ensure that the ['phar' extension](https://www.php.net/manual/en/intro.phar.php) respects the [`SOURCE_DATE_EPOCH`]({{ "/docs/source-date-epoch/" | relative_url }}) environment variable.

* Bernhard M. Wiedemann:

    * [`blobby`](https://build.opensuse.org/request/show/862154) (Zip file embeds creation time)
    * [`dateutils`](https://github.com/hroptatyr/dateutils/issues/121) (fix future build failure, also [filed in openSUSE](https://bugzilla.opensuse.org/show_bug.cgi?id=1180797))
    * [`hamcrest`](https://build.opensuse.org/request/show/860470) (sort list by Pedro Monreal Gonzalez)
    * [`keepalived`](https://github.com/acassen/keepalived/pull/1841) (date issue)
    * [`libcec`](https://github.com/Pulse-Eight/libcec/pull/553) (embeds hostname and user)
    * [`libpinyin`](https://bugzilla.opensuse.org/show_bug.cgi?id=1180734) ([Address space layout randomisation](https://en.wikipedia.org/wiki/Address_space_layout_randomization) issue)
    * [`perf`](https://bugzilla.opensuse.org/show_bug.cgi?id=1180882) (filesystem ordering)
    * [`pythia`](https://build.opensuse.org/request/show/860310) (embeds the build date)
    * [`python-distributed`](https://github.com/dask/distributed/issues/4410) (build fails on single-CPU system)
    * [`python-distributed`](https://github.com/dask/distributed/pull/4403) (build fails in the future due to expired SSL certificate)
    * [`ruby3.0`](https://bugzilla.opensuse.org/show_bug.cgi?id=1180528) (embeds date, process ID, etc.)
    * [`tiptop`](https://build.opensuse.org/request/show/860174) (embeds date and hostname)
    * [`vision`](https://github.com/pytorch/vision/pull/3302) (sort filesystem directory ordering)

* Chris Lamb:

    * [#979134](https://bugs.debian.org/979134) filed against [`apertium-anaphora`](https://tracker.debian.org/pkg/apertium-anaphora).
    * [#980295](https://bugs.debian.org/980295) filed against [`davs2`](https://tracker.debian.org/pkg/davs2).

* Vagrant Cascadian:

    * [#950419](https://bugs.debian.org/950419) filed against [`m4`](https://tracker.debian.org/pkg/m4) (includes date in generated documentation and `.info` files)
    * [#951031](https://bugs.debian.org/951031) filed against [`libtool`](https://tracker.debian.org/pkg/libtool) (remove dates from `.info` and `.html` documentation).
    * [#968627](https://bugs.debian.org/968627) filed against [`libjpeg-turbo`](https://tracker.debian.org/pkg/libjpeg-turbo) (use UTC timestamp as the build date)
    * [#976307](https://bugs.debian.org/976307) filed against [`sudo`](https://tracker.debian.org/pkg/sudo) (different binaries when built on [`/usr`-merged environment](https://wiki.debian.org/UsrMerge)).
    * [#978499](https://bugs.debian.org/978499) filed against [`fop`](https://tracker.debian.org/pkg/fop) (support using [`SOURCE_DATE_EPOCH`](https://reproducible-builds.org/docs/source-date-epoch/) for timestamps in PDF files).
    * [#979019](https://bugs.debian.org/979019), [#979021](https://bugs.debian.org/979021), [#979023](https://bugs.debian.org/979023) and [#979024](https://bugs.debian.org/979024) filed against [`lirc`](https://tracker.debian.org/pkg/lirc).
    * [#979112](https://bugs.debian.org/979112) filed against [`qdbm`](https://tracker.debian.org/pkg/qdbm).
    * [#979125](https://bugs.debian.org/979125) filed against [`gfxboot`](https://tracker.debian.org/pkg/gfxboot) (uploaded to Debian)
    * [#979593](https://bugs.debian.org/979593) filed against [`rox`](https://tracker.debian.org/pkg/rox).
    * [`gfxboot`](https://github.com/openSUSE/gfxboot) ([#49](https://github.com/openSUSE/gfxboot/pull/49)): make the example themes reproducible.

### Testing framework

[![]({{ "/images/reports/2021-01/testframework.png#right" | relative_url }})](https://tests.reproducible-builds.org/)

The Reproducible Builds project operates a large [Jenkins](https://jenkins.io/)-based testing framework that powers [`tests.reproducible-builds.org`](https://tests.reproducible-builds.org). This month, the following changes were made:

* Hans-Christoph Steiner ([F-Droid](https://f-droid.org/)):

    * Include the correct path for debug files.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/77b5205a)]
    * Copy mystery files to `/tmp` for debugging purposes.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/06e09fc5)]
    * Add a call to `git info` to debug why a directory is considered 'dirty'.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/e90e1c2f)]

* Holger Levsen:

    * Debian:

        * Sync copy of *debrebuild* after a [new merge request](https://salsa.debian.org/debian/devscripts/-/merge_requests/212).&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/3571e079)]
        * In the rebuilder tool, enable debugging&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/811a64c5)], call *debrebuild* with `--timestamp=metasnap`&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/60bd1b84)] and deal with `.buildinfo` files being created in current dir&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/1bd8c899)].
        * Add an additional maintainer address for the [Debian Multimedia Maintainers team](https://wiki.debian.org/DebianMultimedia)&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/990356f4)] and update the maintainer address for [OpenStack](https://www.openstack.org/) packages&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/e537e85c)].
        * Don't attempt to install [*diffoscope*](https://diffoscope.org) from *sid* on *stretch*.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/b782fd6d)]

    * Detect diskspace issues on the main Jenkins node.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/ba8de5ae)]
    * In Arch Linux testing, drop support for `.tar.xz` as `pkg.tar.zst` has taken over.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/cf5653ba)]
    * Create a preliminary `README.txt` for [buildinfos.debian.net](https://buildinfos.debian.net).&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/593590f8)]
    * Update hard-coded instance of `2020` - happy new year!&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/56114efe)]

* Johannes Schauer:

    * Update the `README.txt` for the `reproducible_pool_buildinfos.sh` script.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/a7869e93)]

* Mattia Rizzolo:

    * Use a lockfile to ensure builders do not start when not required.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/ee2749b6)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/388d7f6e)]
    * When powercycling `arm64` nodes, use the unprivileged user instead of what is locally configured as 'root'.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/00a0f1aa)]
    * Remove the "Static Analysis Utilities" Jenkins plugin.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/3e01d34f)]
    * Update the deployment tool to set the correct `HOME` environment variable when running Git to avoid printing warnings.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/59e2708d)]
    * Perform a large number of Ubuntu-related configuration changes.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/faeea06e)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/41984710)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/09cc555f)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/17df430d)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/e9a6986b)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/7c0f992b)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/71b6b33a)]
    * Update various host lists.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/206bb5e5)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/e76942f3)]
    * (Re-)add some handy shortcuts.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/59ced5c5)]

Lastly, build node maintenance was performed by Holger Levsen&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/afca8d04)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/6ce40a9b)], Mattia Rizzolo&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/7688d82d)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/4ef2f2fb)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/dd24bed7)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/2b26372c)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/3f7a0b4a)] and Vagrant Cascadian&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/321d77b3)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/6360c735)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/9edbc0db)].

## Community news

Chris Lamb updated the [main Reproducible Builds website and documentation](https://reproducible-builds.org/) including adding a missing image&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/7c32b21)] and updated a script to ignore commits that start with, for example, '`2020 12`' when generating commit listings&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/c95c803)].

On [our mailing list](https://lists.reproducible-builds.org/listinfo/rb-general/) this month, however:

* Fredrik Strömberg [posted to the list](https://lists.reproducible-builds.org/pipermail/rb-general/2021-January/thread.html#2160) to mention that he has been working for many years on establishing trust between end-users and a service operators and infrastructure. In particular, he has been working on a new security architecture called [System Transparency](https://system-transparency.org). Fredrick has previously written two introductory blog posts, including [*System Transparency is the future*](https://mullvad.net/en/blog/2019/6/3/system-transparency-future/) as well as [*Open-source firmware is the future*](https://mullvad.net/nl/blog/2019/8/7/open-source-firmware-future/) which outlines his thinking in more detail, but he intends that System Transparency "enters production use sometime this year".

* Felix C. Stegerman started a discussion around [deterministic Python compiled bytecode files](https://lists.reproducible-builds.org/pipermail/rb-general/2021-January/thread.html#2132) to result in reproducible Android packages which attracted a number of replies. Michael Biebl also got in touch to ask for help on making the [SystemD](https://www.freedesktop.org/wiki/Software/systemd/) package build reproducibility on the various ARM architectures on Debian.&nbsp;[[...](https://lists.reproducible-builds.org/pipermail/rb-general/2021-January/thread.html#2151)]

* We also first wrote about the [Threema](https://threema.ch/) messaging application back in [September 2020]({{ "/reports/2020-09/" | relative_url }}). This month, however, the Threema developers [continued a discussion on our mailing list](https://lists.reproducible-builds.org/pipermail/rb-general/2021-January/thread.html#2161), particularly around applications on various application stores.

* Lastly, David Wheeler asked how we [*how could we accelerate \[the\] deployment of verified reproducible builds?*](https://lists.reproducible-builds.org/pipermail/rb-general/2021-January/thread.html#2171) which was related to another thread continued from December, [*Attack on SolarWinds could have been countered by reproducible builds*](https://lists.reproducible-builds.org/pipermail/rb-general/2021-January/002157.html) regarding how to leverage media coverage to get 'buy in' from upstream developers.

## Contact

If you are interested in contributing to the Reproducible Builds project, please visit our [*Contribute*](https://reproducible-builds.org/contribute/) page on our website. However, you can get in touch with us via:

 * IRC: `#reproducible-builds` on `irc.oftc.net`.

 * Twitter/Mastodon: [@ReproBuilds](https://twitter.com/ReproBuilds) / [@reproducible_builds@fosstodon.org](https://fosstodon.org/@reproducible_builds)

 * Reddit: [/r/ReproducibleBuilds](https://reddit.com/r/reproduciblebuilds)

 * Mailing list: [`rb-general@lists.reproducible-builds.org`](https://lists.reproducible-builds.org/listinfo/rb-general)
