---
layout: report
year: "2022"
month: "01"
title: "Reproducible Builds in January 2022"
draft: false
date: 2022-02-05 19:08:21
---

[![]({{ "/images/reports/2022-01/reproducible-builds.png#right" | relative_url }})](https://reproducible-builds.org/)

**Welcome to the January 2022 report from the [Reproducible Builds](https://reproducible-builds.org) project.** In our reports, we try outline the most important things that have been happening in the past month. As ever, if you are interested in contributing to the project, please visit our [*Contribute*]({{ "/contribute/" | relative_url }}) page on our website.

<br>

[![]({{ "/images/reports/2022-01/gossamer.png#right" | relative_url }})](https://paragonie.com/blog/2022/01/solving-open-source-supply-chain-security-for-php-ecosystem)

An interesting blog post was published by [Paragon Initiative Enterprises](https://paragonie.com/) about [*Gossamer*](https://gossamer.tools/), a proposal for securing the PHP software supply-chain. Utilising code-signing and third-party attestations, Gossamer aims to mitigate the risks within the notorious PHP world via publishing attestations to a transparency log. Their post, titled [*Solving Open Source Supply Chain Security for the PHP Ecosystem*](https://paragonie.com/blog/2022/01/solving-open-source-supply-chain-security-for-php-ecosystem) goes into some detail regarding the design, scope and implementation of the system.

<br>

[![]({{ "/images/reports/2022-01/supplychainsecuritycon.png#right" | relative_url }})](https://events.linuxfoundation.org/open-source-summit-north-america/about/supplychainsecuritycon/)

This month, the [Linux Foundation](https://linuxfoundation.org) announced [SupplyChainSecurityCon](https://events.linuxfoundation.org/open-source-summit-north-america/about/supplychainsecuritycon/), a conference focused on exploring the security threats affecting the software supply chain, sharing best practices and mitigation tactics. The conference is part of the Linux Foundation's [Open Source Summit North America](https://events.linuxfoundation.org/open-source-summit-north-america/) and will take place June 21st — 24th 2022, both virtually and in Austin, Texas.

<br>

## Debian

[![]({{ "/images/reports/2022-01/debian.png#right" | relative_url }})](https://debian.org/)

There was a significant progress made in the [Debian](https://debian.org/) Linux distribution this month, including:

* Roland Clobus continued work on reproducible 'live' images in the past month, and some of his work [was merged into *live-build* itself](https://salsa.debian.org/live-team/live-build/-/commit/a599f50e480e8755d61f515c4351ee2dd33d89a3). The [ReproducibleInstalls/LiveImages](https://wiki.debian.org/ReproducibleInstalls/LiveImages) page on the [Debian Wiki](https://wiki.debian.org/) as well as the existing/custom Jenkins hooks [were updated to match](https://salsa.debian.org/qa/jenkins.debian.net/-/commit/19113c3cd557aed95807a868b477a84c75f8bb57o).

* Related to this, it is now possible to create a bit-by-bit reproducible chroots using [`mmdebstrap`](https://tracker.debian.org/pkg/mmdebstrap) when `SOURCE_DATE_EPOCH` is set. As of Debian 11 Bullseye, this works for all variants, except for the variant that includes all so-called '`Priority: standard`' packages where `fontconfig` caches, `*.pyc` files and man-db `index.db` are unreproducible. (These issues have been addressed in [Tails](https://tails.boum.org/) and *live-build* by [removing](https://gitlab.tails.boum.org/tails/tails/-/blob/stable/config/chroot_local-hooks/99-remove_pyc) [`*.pyc` files](https://salsa.debian.org/live-team/live-build/-/blob/master/share/hooks/normal/0170-remove-python-py.hook.chroot), in addition to [removing](https://gitlab.tails.boum.org/tails/tails/-/blob/stable/config/chroot_local-hooks/99-zzzzzz_reproducible-builds-post-processing#L28) [`index.db`](https://salsa.debian.org/live-team/live-build/-/blob/master/share/hooks/normal/0190-remove-temporary-files.hook.chroot#L6) files as well. (Whilst `index.db` files can be regenerated, there is no straightforward method to re-creating `*.pyc` files, and the resulting installation will suffer from reduced performance of Python scripts. Ideally, no removal would be necessary as all files would be created reproducibly in the first place.)

* Further to this work, Roland wrote up a [comprehensive status update about *live-build* ISO images](https://lists.reproducible-builds.org/pipermail/rb-general/2022-January/002472.html) to [our mailing list](https://lists.reproducible-builds.org/listinfo/rb-general/) as well.

* The [PackageRebuilder](https://github.com/fepitre/package-rebuilder) instance running at [`beta.tests.reproducible-builds.org`](https://beta.tests.reproducible-builds.org/) is now aware/capable of [building packages for Debian *bookworm*](https://beta.tests.reproducible-builds.org/debian.html#debian-bookworm-amd64).

* A longstanding issue around [`fontconfig`](https://tracker.debian.org/pkg/fontconfig)'s cache files being unreproducible saw some progress this month. Debian bug number [#864082](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=864082) (originally filed in June 2017) was NMU'd by *josch*, although this resulted in a [small number of minor side-effects](https://bugs.debian.org/864082#483) which have already been addressed with a follow-up patch.

* 120 reviews of Debian packages were added, 272 were updated and 31 were removed this month, all adding to [our index of identified issues](https://tests.reproducible-builds.org/debian/index_issues.html). A number of issue types were updated too [[...](https://salsa.debian.org/reproducible-builds/reproducible-notes/commit/4b8c998c)][[...](https://salsa.debian.org/reproducible-builds/reproducible-notes/commit/fa00c765)][[...](https://salsa.debian.org/reproducible-builds/reproducible-notes/commit/164d61f6)][[...](https://salsa.debian.org/reproducible-builds/reproducible-notes/commit/a829a2fe)].

* [*kpcyrd*](https://vulns.xyz/) blogged this month about Debian binary NMUs and `.buildinfo` files in a post entitled, [*Reproducible Builds: Debian and the case of the missing version string*](https://vulns.xyz/2022/01/debian-missing-version-string/).

<br>

### Other distributions

[![]({{ "/images/reports/2022-01/archlinux.png#right" | relative_url }})](https://archlinux.org)

[*kpcyrd*](https://vulns.xyz/) [reported on Twitter](https://twitter.com/kpcyrd/status/1477662064625827841) about the release of version 0.2.0 of [*pacman-bintrans*](https://github.com/kpcyrd/pacman-bintrans), an experiment with binary transparency for the [Arch Linux](https://archlinux.org/) package manager, [*pacman*](https://wiki.archlinux.org/title/pacman). This new version is now able to query [*rebuilderd*](https://github.com/kpcyrd/rebuilderd) to check if a package was independently reproduced.

<br>

In the world of [openSUSE](https://www.opensuse.org/), however, Bernhard M. Wiedemann posted his [monthly reproducible builds status report](https://lists.opensuse.org/archives/list/factory@lists.opensuse.org/thread/JCM3YKDLS6XVE3H2TFZKKXZX3WBSNCM2/).

<br>

## [*diffoscope*](https://diffoscope.org)

[![]({{ "/images/reports/2022-01/diffoscope.svg#right" | relative_url }})](https://diffoscope.org)

[*diffoscope*](https://diffoscope.org) is our in-depth and content-aware diff utility. Not only can it locate and diagnose reproducibility issues, it can provide human-readable diffs from many kinds of binary formats. This month, Chris Lamb prepared and uploaded versions [`199`](https://diffoscope.org/news/diffoscope-199-released/), [`200`](https://diffoscope.org/news/diffoscope-200-released/), [`201`](https://diffoscope.org/news/diffoscope-201-released/) and [`202`](https://diffoscope.org/news/diffoscope-201-released/) to Debian *unstable* (that were later backported to Debian *bullseye-backports* by Mattia Rizzolo), as well as made the following changes to the code itself:

* New features:

    * First attempt at incremental output support with a timeout. Now passing, for example, `--timeout=60` will mean that diffoscope will not recurse into any sub-archives after 60 seconds total execution time has elapsed. Note that this is not a fixed/strict timeout due to implementation issues. [[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/e99851bf)][[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/3b515cc5)]
    * Support both variants of `odt2txt`, including the one provided by the `unoconv` package. [[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/0f45b185)]

* Bug fixes:

    * Do not return with a UNIX exit code of 0 if we encounter with a file whose human-readable metadata matches literal file contents. [[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/6ae98df8)]
    * Don't fail if comparing a nonexistent file with a `.pyc` file (and add test). [[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/9ebba4b2)][[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/a3abe03d)]
    * If the `debian.deb822` module raises any exception on import, re-raise it as an `ImportError`. This should fix diffoscope on some Fedora systems. [[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/c082b52f)]
    * Even if a Sphinx `.inv` inventory file is labelled *The remainder of this file is compressed using zlib*, it might not actually be. In this case, don't traceback and simply return the original content. [[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/6eb56487)]

* Documentation:

    * Improve documentation for the new `--timeout` option due to a few misconceptions. [[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/21fc4d7d)]
    * Drop reference in the manual page claiming the ability to compare non-existent files on the command-line. (This has not been possible since version 32 which was released in September 2015). [[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/b51a31e9)]
    * Update '*X has been modified after `NT_GNU_BUILD_ID` has been applied*' messages to, for example, not duplicating the full filename in the diffoscope output. [[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/ae452baf)]

* Codebase improvements:

    * Tidy some control flow. [[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/7b86483c)]
    * Correct a 'recompile' typo. [[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/3ca5d02e)]

In addition, Alyssa Ross fixed the comparison of CBFS names that contain spaces&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/5269c1c0)],
Sergei Trofimovich fixed whitespace for compatibility with version 21.12 of the [Black source code reformatter](https://github.com/psf/black)&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/fba70d27)] and Zbigniew Jędrzejewski-Szmek fixed JSON detection with a new version of [file](http://www.darwinsys.com/file/)&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/0c124437)].

<br>

## Testing framework

[![]({{ "/images/reports/2022-01/testframework.png#right" | relative_url }})](https://tests.reproducible-builds.org/)

The Reproducible Builds project runs a significant testing framework at [tests.reproducible-builds.org](https://tests.reproducible-builds.org), to check packages and other artifacts for reproducibility. This month, the following changes were made:

* Frédéric Pierret (*fepitre*):

    * Add Debian *bookworm* to package set creation.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/3ee2ae31)]

* Holger Levsen:

    * Install the `po4a` package where appropriate, as it is needed for the Reproducible Builds website job&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/1492be0e)]. In addition, also run the `i18n.sh` and `contributors.sh` scripts [[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/6c2c080e)].
    * Correct some grammar in Debian 'live' image build output.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/00312916)]
    * Shell monitor improvements:
        * Only show the 'offline node' section if there are offline nodes.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/7a3e34ae)]
        * Colorise offline nodes.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/f13ca601)]
        * Shrink screen usage.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/2c557234)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/552eb379)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/eb67bb02)]

    * Node health check improvements:
        * Detect if 'live' package builds encounter incomplete snapshots.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/bf8dd46d)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/27a1ffd8)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/75d7cc53)]
        * Detect if a host is running with today's date (when it should be set artificially in the future).&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/88c129ac)]
    * Use the `devscripts` package from *bullseye-backports* on Debian nodes.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/c32dc41c)]
    * Use the [Munin monitoring](https://munin-monitoring.org/) package *bullseye-backports* on Debian nodes too.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/e396b83a)]
    * Update 'New Year' handling, needed to be able to detect 'real' and fake dates.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/17746273)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/074128b6)]
    * Improve the error message of the script that powercycles the `arm64` architecture nodes hosted by [Codethink](codethink.co.uk).&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/1fa569eb)]

* Mattia Rizzolo:

    * Use the new `--timeout` option added in *diffoscope* version 202.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/f01bc667)]

* Roland Clobus:

    * Update the build scripts now that the hooks for 'live' builds are now maintained upstream in the *live-build* repository.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/19113c3c)]
    * Show 'info' lines in Jenkins when reproducible hooks have been active.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/5e0a3361)]
    * Use unique folders for the artifacts from each 'live' Debian version.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/8a1db551)]

* Vagrant Cascadian:

    * Switch the Debian `armhf` architecture nodes to use new proxy.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/68d22bdb)]
    * Misc. node maintenance.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/b7bd204e)].

<br>

## Upstream patches

The Reproducible Builds project attempts to fix as many currently-unreproducible packages as possible. In January, we wrote a large number of such patches, including:

* Leonidas Spyropoulos:

    * [`freeplane`](https://github.com/freeplane/freeplane/pull/248) (timestamps, file order)
    * [`opensearch`](https://github.com/opensearch-project/OpenSearch/pull/1995) (timestamps, file order)

* Bernhard M. Wiedemann:

    * [`cosign`](https://build.opensuse.org/request/show/948967) (date)
    * [`gnome-todo`](https://gitlab.gnome.org/GNOME/gnome-todo/-/merge_requests/110) (discover fix for single-CPU build failure)
    * [`gstreamer-plugins-good`](https://gitlab.freedesktop.org/gstreamer/gst-plugins-bad/-/issues/1697) (build fails without debuginfo)
    * [`jogl2`](https://build.opensuse.org/request/show/949600) (parallelism-related issue)
    * [`libkolabxml`](https://build.opensuse.org/request/show/943530) (ASLR)
    * [`monero-gui`](https://build.opensuse.org/request/show/949547) (CPU)
    * [`python-pyudev`](https://build.opensuse.org/request/show/948959) (date)
    * [`rekor`](https://build.opensuse.org/request/show/948951) (date)
    * [`rivet`](https://gitlab.com/hepcedar/rivet/-/issues/255) (FTBFS-j1 has unreleased fix upstream)
    * [`skaffold`](https://build.opensuse.org/request/show/949884) (date)

* Chris Lamb:

    * [#1003159](https://bugs.debian.org/1003159) filed against [`dh-raku`](https://tracker.debian.org/pkg/dh-raku).
    * [#1003203](https://bugs.debian.org/1003203) filed against [`libgtkdatabox`](https://tracker.debian.org/pkg/libgtkdatabox).
    * [#1003385](https://bugs.debian.org/1003385) filed against [`qcelemental`](https://tracker.debian.org/pkg/qcelemental).
    * [#1003646](https://bugs.debian.org/1003646) filed against [`node-ramda`](https://tracker.debian.org/pkg/node-ramda).
    * [#1003929](https://bugs.debian.org/1003929) filed against [`ncurses`](https://tracker.debian.org/pkg/ncurses).
    * [#1004391](https://bugs.debian.org/1004391) filed against [`python-fluids`](https://tracker.debian.org/pkg/python-fluids) ([forwarded upstream](https://github.com/CalebBell/fluids/pull/46)).
    * [#1004676](https://bugs.debian.org/1004676) filed against [`node-istanbul`](https://tracker.debian.org/pkg/node-istanbul).

* Johannes Schauer Marin Rodrigues:

    * [MR !9](https://salsa.debian.org/debian/dash/-/merge_requests/9) filed against [`dash`](https://tracker.debian.org/pkg/dash)
    * [#1004557](https://bugs.debian.org/1004557) filed against [`dpkg`](https://tracker.debian.org/pkg/dpkg).
    * [#1004558](https://bugs.debian.org/1004558) filed against [`python3.10`](https://tracker.debian.org/pkg/python3.10).

* Roland Clobus:

    * [#1003449](https://bugs.debian.org/1003449) filed against [`texlive-base`](https://tracker.debian.org/pkg/texlive-base).

* Vagrant Cascadian:

    * [#1003316](https://bugs.debian.org/1003316) filed against [`python-cooler`](https://tracker.debian.org/pkg/python-cooler).
    * [#1003319](https://bugs.debian.org/1003319) filed against [`insighttoolkit5`](https://tracker.debian.org/pkg/insighttoolkit5).
    * [#1003368](https://bugs.debian.org/1003368) filed against [`dino-im`](https://tracker.debian.org/pkg/dino-im).
    * [#1003370](https://bugs.debian.org/1003370) filed against [`libxtrxll`](https://tracker.debian.org/pkg/libxtrxll).
    * [#1003371](https://bugs.debian.org/1003371) filed against [`libavif`](https://tracker.debian.org/pkg/libavif).
    * [#1003373](https://bugs.debian.org/1003373) filed against [`go-for-it`](https://tracker.debian.org/pkg/go-for-it).
    * [#1003375](https://bugs.debian.org/1003375) filed against [`clfft`](https://tracker.debian.org/pkg/clfft).
    * [#1003379](https://bugs.debian.org/1003379) filed against [`last-align`](https://tracker.debian.org/pkg/last-align).
    * [#1003430](https://bugs.debian.org/1003430) filed against [`gkrellm-leds`](https://tracker.debian.org/pkg/gkrellm-leds).
    * [#1003488](https://bugs.debian.org/1003488) and [#1003489](https://bugs.debian.org/1003489) filed against [`last-align`](https://tracker.debian.org/pkg/last-align).
    * [#1003494](https://bugs.debian.org/1003494) filed against [`binutils-xtensa-lx106`](https://tracker.debian.org/pkg/binutils-xtensa-lx106).
    * [#1003495](https://bugs.debian.org/1003495) filed against [`gcc-xtensa-lx106`](https://tracker.debian.org/pkg/gcc-xtensa-lx106).
    * [#1003500](https://bugs.debian.org/1003500) and [#1003501](https://bugs.debian.org/1003501) filed against [`gcc-sh-elf`](https://tracker.debian.org/pkg/gcc-sh-elf).
    * [#1003787](https://bugs.debian.org/1003787) filed against [`pesign`](https://tracker.debian.org/pkg/pesign).
    * [#1003802](https://bugs.debian.org/1003802) filed against [`upb`](https://tracker.debian.org/pkg/upb).
    * [#1003803](https://bugs.debian.org/1003803) filed against [`paho.mqtt.c`](https://tracker.debian.org/pkg/paho.mqtt.c).
    * [#1003804](https://bugs.debian.org/1003804) filed against [`imath`](https://tracker.debian.org/pkg/imath).
    * [#1003808](https://bugs.debian.org/1003808) filed against [`libvcflib`](https://tracker.debian.org/pkg/libvcflib).
    * [#1003809](https://bugs.debian.org/1003809) filed against [`pkg-js-tools`](https://tracker.debian.org/pkg/pkg-js-tools).
    * [#1003912](https://bugs.debian.org/1003912) filed against [`vdeplug4`](https://tracker.debian.org/pkg/vdeplug4).
    * [#1003914](https://bugs.debian.org/1003914) filed against [`kget`](https://tracker.debian.org/pkg/kget).
    * [#1003915](https://bugs.debian.org/1003915) filed against [`mathgl`](https://tracker.debian.org/pkg/mathgl).
    * [#1003919](https://bugs.debian.org/1003919) filed against [`apulse`](https://tracker.debian.org/pkg/apulse).
    * [#1003920](https://bugs.debian.org/1003920) filed against [`akonadi`](https://tracker.debian.org/pkg/akonadi).
    * [#1003922](https://bugs.debian.org/1003922) filed against [`recastnavigation`](https://tracker.debian.org/pkg/recastnavigation).
    * [#1003923](https://bugs.debian.org/1003923) filed against [`soundkonverter`](https://tracker.debian.org/pkg/soundkonverter).
    * [#1003924](https://bugs.debian.org/1003924) filed against [`indi`](https://tracker.debian.org/pkg/indi).
    * [#1003978](https://bugs.debian.org/1003978) filed against [`akonadi-mime`](https://tracker.debian.org/pkg/akonadi-mime).
    * [#1003980](https://bugs.debian.org/1003980) filed against [`akonadi-import-wizard`](https://tracker.debian.org/pkg/akonadi-import-wizard).
    * [#1003992](https://bugs.debian.org/1003992) filed against [`akonadi-contacts`](https://tracker.debian.org/pkg/akonadi-contacts).
    * [#1003993](https://bugs.debian.org/1003993) filed against [`cryptominisat`](https://tracker.debian.org/pkg/cryptominisat).
    * [#1003995](https://bugs.debian.org/1003995) filed against [`hipercontracer`](https://tracker.debian.org/pkg/hipercontracer).
    * [#1003997](https://bugs.debian.org/1003997) filed against [`libksba`](https://tracker.debian.org/pkg/libksba).
    * [#1003999](https://bugs.debian.org/1003999) filed against [`leatherman`](https://tracker.debian.org/pkg/leatherman).
    * [#1004002](https://bugs.debian.org/1004002) filed against [`segyio`](https://tracker.debian.org/pkg/segyio).
    * [#1004004](https://bugs.debian.org/1004004) filed against [`darkradiant`](https://tracker.debian.org/pkg/darkradiant).
    * [#1004005](https://bugs.debian.org/1004005) filed against [`openmm`](https://tracker.debian.org/pkg/openmm).
    * [#1004034](https://bugs.debian.org/1004034) filed against [`bagel`](https://tracker.debian.org/pkg/bagel).
    * [#1004053](https://bugs.debian.org/1004053) filed against [`kallisto`](https://tracker.debian.org/pkg/kallisto).
    * [#1004057](https://bugs.debian.org/1004057) filed against [`evolution-ews`](https://tracker.debian.org/pkg/evolution-ews).

<br>

## And finally...

If you are interested in contributing to the Reproducible Builds project, please visit our [*Contribute*](https://reproducible-builds.org/contribute/) page on our website. However, you can get in touch with us via:

 * IRC: `#reproducible-builds` on `irc.oftc.net`.

 * Twitter: [@ReproBuilds](https://twitter.com/ReproBuilds)

 * Mailing list: [`rb-general@lists.reproducible-builds.org`](https://lists.reproducible-builds.org/listinfo/rb-general)
