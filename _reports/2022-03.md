---
layout: report
year: "2022"
month: "03"
title: "Reproducible Builds in March 2022"
draft: false
date: 2022-04-08 08:14:15
---

[![]({{ "/images/reports/2022-03/reproducible-builds.png#right" | relative_url }})](https://reproducible-builds.org/)

**Welcome to the March 2022 report from the [Reproducible Builds](https://reproducible-builds.org) project!** In our monthly reports we outline the most important things that we have been up to over the past month.

<br>

[![]({{ "/images/reports/2022-03/intoto.png#right" | relative_url }})](https://www.cncf.io/blog/2022/03/10/supply-chain-security-project-in-toto-moves-to-the-cncf-incubator/)

The [*in-toto*](https://in-toto.io/) project was accepted as an "incubating project" within the [Cloud Native Computing Foundation](https://www.cncf.io/) (CNCF). *in-toto* is a framework that protects the software supply chain by collecting and verifying relevant data. It does so by enabling libraries to collect information about software supply chain actions and then allowing software users and/or project managers to publish policies about software supply chain practices that can be verified before deploying or installing software. CNCF foundations hosts a number of [critical components of the global technology infrastructure](https://www.cncf.io/projects/) under the auspices of the [Linux Foundation](https://www.linuxfoundation.org). (View [full announcement](https://www.cncf.io/blog/2022/03/10/supply-chain-security-project-in-toto-moves-to-the-cncf-incubator/).)

<br>

[![]({{ "/images/reports/2022-03/reproducible-central.png#right" | relative_url }})](https://github.com/jvm-repo-rebuild/reproducible-central)

Hervé Boutemy [posted to our mailing list with an announcement](https://lists.reproducible-builds.org/pipermail/rb-general/2022-March/002529.html) that the Java Reproducible Central has hit the milestone of "500 fully reproduced builds of upstream projects". Indeed, at the time of writing, according to the [nightly rebuild results](https://github.com/jvm-repo-rebuild/reproducible-central), 530 releases were found to be fully reproducible, with 100% reproducible artifacts.

<br>

[![]({{ "/images/reports/2022-03/gitbom.png#right" | relative_url }})](https://github.com/git-bom/bomsh#Reproducible-Build-and-Bomsh)

[GitBOM](https://gitbom.dev/) is relatively new project to enable build tools trace every source file that is incorporated into build artifacts. As an experiment and/or proof-of-concept, the GitBOM developers are rebuilding Debian to generate side-channel build metadata for versions of Debian that have already been released. This only works because Debian is (partial) reproducible, so one can be sure that that, if the case where build artifacts are identical, any metadata generated during these instrumented builds applies to the binaries that were built and released in the past. More information on their approach is available in [`README` file in the *bomsh* repository](https://github.com/git-bom/bomsh#Reproducible-Build-and-Bomsh).

<br>

Ludovic Courtes has published an academic paper discussing how the performance requirements of high-performance computing are not (as usually assumed) at odds with reproducible builds. The received wisdom is that vendor-specific libraries and platform-specific CPU extensions have resulted in a culture of local recompilation to ensure the best performance, rendering the property of reproducibility unobtainable or even meaningless. In his paper, Ludovic explains how Guix has:

> [...] implemented what we call “package multi-versioning” for C/C++ software that lacks function multi-versioning and run-time dispatch [...]. It is another way to ensure that users do not have to trade reproducibility for performance. ([full PDF](https://arxiv.org/pdf/2203.07953.pdf))

<br>

[![]({{ "/images/reports/2022-03/three-pillars.png#right" | relative_url }})](https://fossa.com/blog/three-pillars-reproducible-builds/)

Kit Martin posted to the [FOSSA](https://fossa.com/) blog a post titled [*The Three Pillars of Reproducible Builds*](https://fossa.com/blog/three-pillars-reproducible-builds/). Inspired by the "shock of infiltrated or intentionally broken NPM packages, supply chain attacks, long-unnoticed backdoors", the post goes on to outline the high-level steps that lead to a reproducible build:

> It is one thing to talk about reproducible builds and how they strengthen software supply chain security, but it’s quite another to effectively configure a reproducible build. Concrete steps for specific languages are a far larger topic than can be covered in a single blog post, but today we’ll be talking about some guiding principles when designing reproducible builds. [[...](https://fossa.com/blog/three-pillars-reproducible-builds/)]

The article was [discussed on Hacker News](https://news.ycombinator.com/item?id=30604954).

<br>

[![]({{ "/images/reports/2022-03/werewolves.png#right" | relative_url }})](https://github.com/rajeshsola/gnu-hello/blob/master/tests/greeting-2#18)

Finally, Bernhard M. Wiedemann noticed that the GNU *Helloworld* project varies depending on whether it is being [built during a full moon](https://github.com/rajeshsola/gnu-hello/blob/master/tests/greeting-2#L18)! ([Reddit announcement](https://old.reddit.com/r/reproduciblebuilds/comments/tqrf9q/the_binary_that_varies_from_full_moon/), [openSUSE bug report](https://bugzilla.opensuse.org/show_bug.cgi?id=1197575))

<br>

## Events

[![]({{ "/images/reports/2022-03/debian-hamburg.png#right" | relative_url }})](https://wiki.debian.org/DebianEvents/de/2022/DebianReunionHamburg)

There will be an in-person "Debian Reunion" in Hamburg, Germany later this year, taking place from 23 — 30 May. Although this is a "Debian" event, there will be some folks from the broader Reproducible Builds community and, of course, everyone is welcome. Please see the [event page on the Debian wiki](https://wiki.debian.org/DebianEvents/de/2022/DebianReunionHamburg) for more information.

Bernhard M. Wiedemann posted to our mailing list about a [meetup for Reproducible Builds folks at the openSUSE conference in Nuremberg, Germany](https://lists.reproducible-builds.org/pipermail/rb-general/2022-March/002509.html).

[![]({{ "/images/reports/2022-03/debconf22.png#right" | relative_url }})](https://debconf22.debconf.org/)

It was also recently announced that [DebConf22](https://debconf22.debconf.org/) will [take place this year as an in-person conference](https://debconf22.debconf.org/news/2022-03-09-debconf22-in-person/) in Prizren, Kosovo. The pre-conference meeting (or "Debcamp") will take place from 10 — 16 July, and the main talks, workshops, etc. will take place from 17 — 24 July.

## Misc news

Holger Levsen updated the Reproducible Builds website to improve the [documentation for the `SOURCE_DATE_EPOCH` environment variable]({{ "/docs/source-date-epoch/" | relative_url }}), both by expanding parts of the existing text&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/3764949f)][[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/013a11fb)] as well as clarifying meaning by removing text in other places&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/8d3ecd99)]. In addition, Chris Lamb added a [Twitter Card](https://developer.twitter.com/en/docs/twitter-for-websites/cards/overview/abouts-cards) to [our website](https://reproducible-builds.org/)'s metadata too [[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/7153dab5)][[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/0cd478a6)][[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/d08be58d)].

On [our mailing list](https://lists.reproducible-builds.org/listinfo/rb-general/) this month:

* Early in the month, Mattia Rizzolo posted to our mailing list asking for [early thoughts about running an in-person Reproducible Builds event](https://lists.reproducible-builds.org/pipermail/rb-general/2022-March/002491.html) later in 2022.

* Chris Lamb then posted to our mailing list with a [call for "real-world instances" where reproducibility practices have flagged something legitimately "bad"](https://lists.reproducible-builds.org/pipermail/rb-general/2022-March/002516.html). Although no public, concrete examples were cited, the resulting discussion was interesting and wide-ranging.

* Marc Haber also posted to our mailing list with an interesting problem where [building Debian packages with *libfaketime* yields different results when run outside of *libfaketime*](https://lists.reproducible-builds.org/pipermail/rb-general/2022-March/002513.html).

<br>

## Distribution work

In Debian this month:

[![]({{ "/images/reports/2022-03/debian.png#right" | relative_url }})](https://debian.org/)

* Johannes Schauer Marin Rodrigues posted to the [*debian-devel*](https://lists.debian.org/debian-devel/) list mentioning that he exploited the property of reproducibility within Debian to demonstrate that automatically converting a large number of packages to a new internal "source version" did not change the resulting packages. The proposed change could therefore be applied without causing breakage:

> So now we have 364 source packages for which we have a patch and for which we can show that this patch does not change the build output. Do you agree that with those two properties, the advantages of the 3.0 (quilt) format are sufficient such that the change shall be implemented at least for those 364? [[...](https://lists.debian.org/debian-devel/2022/03/msg00096.html)]

* 144 reviews of Debian packages were added, 241 were updated and 20 were removed this month, significantly adding to [our knowledge about identified issues](https://tests.reproducible-builds.org/debian/index_issues.html). A number of issue types were updated too, including [`buildpath_in_postgres_opcodes`](https://salsa.debian.org/reproducible-builds/reproducible-notes/commit/7a6e9a65), [`captures_kernel_version_via_CMAKE_SYSTEM`](https://salsa.debian.org/reproducible-builds/reproducible-notes/commit/09d0eba5), [`build_id_differences_only`](https://salsa.debian.org/reproducible-builds/reproducible-notes/commit/7b01a140), etc.

* Lukas Puehringer updated both the `python-securesystemslib` package to version `0.22.0-1` and the `in-toto` package to `1.2.0-1`.

[![]({{ "/images/reports/2022-03/opensuse.png#right" | relative_url }})](https://www.opensuse.org/)

In [openSUSE](https://www.opensuse.org/), Bernhard M. Wiedemann posted his [usual monthly reproducible builds status report](https://lists.opensuse.org/archives/list/factory@lists.opensuse.org/thread/HQXX6XKNFZ2ERNXGK2QDZVZ5C4Y4SBPS/).

<br>

## Tooling

[![]({{ "/images/reports/2022-03/diffoscope.svg#right" | relative_url }})](https://diffoscope.org)

[*diffoscope*](https://diffoscope.org) is our in-depth and content-aware diff utility. Not only can it locate and diagnose reproducibility issues, it can provide human-readable diffs from many kinds of binary formats. This month, Chris Lamb prepared and uploaded versions [`207`](https://diffoscope.org/news/diffoscope-207-released/), [`208`](https://diffoscope.org/news/diffoscope-203-released/) and [`209`](https://diffoscope.org/news/diffoscope-209-released/) to Debian *unstable*, as well as made the following changes to the code itself:

* Update minimum version of [Black](https://black.readthedocs.io/en/stable/) to prevent test failure on Ubuntu *jammy*.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/c40a131b)]

* Updated the R test fixture for the 4.2.x series of the [R programming language](https://www.r-project.org/).&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/68638cd3)]

Brent Spillner also worked on adding graceful handling for UNIX sockets and named pipes to *diffoscope*.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/14e21db2)][[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/490a4b4c)][[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/26a50176)]. Vagrant Cascadian also updated the *diffoscope* package in [GNU Guix](https://www.gnu.org/software/guix/).&nbsp;[[...](https://git.savannah.gnu.org/cgit/guix.git/commit/?id=137a5bc71de8f90ec76f2321294d02620234ae66)][[...](https://git.savannah.gnu.org/cgit/guix.git/commit/?id=af8d568f82a677640ba6cc3228a896082a14aac3)]

[*reprotest*](https://tracker.debian.org/pkg/reprotest) is the Reproducible Build's project end-user tool to build the same source code twice in widely different environments and checking whether the binaries produced by the builds have any differences. This month, Santiago Ruano Rincón added a new `--append-build-command` option&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reprotest/commit/4f83ce1)], which was subsequently uploaded to Debian *unstable* by Holger Levsen.

<br>

#### Upstream patches

The Reproducible Builds project detects, dissects and attempts to fix as many currently-unreproducible packages as possible. We endeavour to send all of our patches upstream where appropriate. This month, we wrote a large number of such patches, including:

* Bernhard M. Wiedemann:

    * [`epub2txt2`](https://github.com/kevinboone/epub2txt2/pull/20) (filesystem-related issue)
    * [`fdupes`](https://bugzilla.opensuse.org/show_bug.cgi?id=1197484) (toolchain issue)
    * [`grep`](https://build.opensuse.org/request/show/962124) ([ASLR](https://en.wikipedia.org/wiki/Address_space_layout_randomization)-related issue)
    * [`hello`](https://bugzilla.opensuse.org/show_bug.cgi?id=1197575) ([PGO](https://en.wikipedia.org/wiki/Profile-guided_optimization)-related issue)
    * [`osdlyrics`](https://github.com/osdlyrics/osdlyrics/issues/110) (date-related regression regression)
    * [`texlive`](https://build.opensuse.org/request/show/966045) (GZip modification time issue)
    * [`tuigreet`](https://github.com/apognu/tuigreet/issues/60) (ordering issue)

* Huong Nguyenthi:

    * [#1006646](https://bugs.debian.org/1006646) filed against [`sgml-base`](https://tracker.debian.org/pkg/sgml-base).

* Chris Lamb:

    * [#1007232](https://bugs.debian.org/1007232) filed against [`python-ara`](https://tracker.debian.org/pkg/python-ara).
    * [#1007757](https://bugs.debian.org/1007757) filed against [`nbformat`](https://tracker.debian.org/pkg/nbformat).
    * [#1007760](https://bugs.debian.org/1007760) filed against [`chemical-structures`](https://tracker.debian.org/pkg/chemical-structures).
    * [#1007908](https://bugs.debian.org/1007908) filed against [`fiat`](https://tracker.debian.org/pkg/fiat).

* Vagrant Cascadian:
    * [#1006844](https://bugs.debian.org/1006844) filed against [`intel-mediasdk`](https://tracker.debian.org/pkg/intel-mediasdk).
    * [#1006858](https://bugs.debian.org/1006858) filed against [`libao`](https://tracker.debian.org/pkg/libao).
    * [#1006860](https://bugs.debian.org/1006860) & [#1006861](https://bugs.debian.org/1006861) filed against [`pcp`](https://tracker.debian.org/pkg/pcp).
    * [#1006863](https://bugs.debian.org/1006863) filed against [`tevent`](https://tracker.debian.org/pkg/tevent).
    * [#1006864](https://bugs.debian.org/1006864) filed against [`pcp`](https://tracker.debian.org/pkg/pcp).
    * [#1006865](https://bugs.debian.org/1006865) filed against [`apr-util`](https://tracker.debian.org/pkg/apr-util).
    * [#1006979](https://bugs.debian.org/1006979) filed against [`liggghts`](https://tracker.debian.org/pkg/liggghts).
    * [#1007094](https://bugs.debian.org/1007094) filed against [`kristall`](https://tracker.debian.org/pkg/kristall).
    * [#1007095](https://bugs.debian.org/1007095) filed against [`lmod`](https://tracker.debian.org/pkg/lmod).
    * [#1007137](https://bugs.debian.org/1007137) filed against [`libranlip`](https://tracker.debian.org/pkg/libranlip).
    * [#1007184](https://bugs.debian.org/1007184) filed against [`xrt`](https://tracker.debian.org/pkg/xrt).
    * [#1007185](https://bugs.debian.org/1007185) filed against [`btrfsmaintenance`](https://tracker.debian.org/pkg/btrfsmaintenance).


<br>

## Testing framework

[![]({{ "/images/reports/2022-03/testframework.png#right" | relative_url }})](https://tests.reproducible-builds.org/)

The Reproducible Builds project runs a significant testing framework at [tests.reproducible-builds.org](https://tests.reproducible-builds.org), to check packages and other artifacts for reproducibility. This month, the following changes were made:

* Holger Levsen:

    * Replace a local copy of the `dsa-check-running-kernel` script with a packaged version.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/264b546d)]
    * Don't hide the status of offline hosts in the Jenkins shell monitor.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/a3768d4d)]
    * Detect undefined service problems in the node health check.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/04d457ce)]
    * Update the `sources.lst` file for our mail server as its still running Debian *buster*.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/739be49c)]
    * Add our mail server to our node inventory so it is included in the Jenkins maintenance processes.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/dd0a7d98)]
    * Remove the `debsecan` package everywhere; it got installed accidentally via the `Recommends` relation.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/5ec458ad)]
    * Document the usage of the *osuosl174* host.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/66617cd8)]

Regular node maintenance was also performed by Holger Levsen [[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/dd8311e2)], Vagrant Cascadian [[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/9804bd52)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/ad3ee88d)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/f88a445f)] and Mattia Rizzolo.

<br>

If you are interested in contributing to the Reproducible Builds project, please visit our [*Contribute*](https://reproducible-builds.org/contribute/) page on our website. However, you can get in touch with us via:

 * IRC: `#reproducible-builds` on `irc.oftc.net`.

 * Twitter: [@ReproBuilds](https://twitter.com/ReproBuilds)

 * Mailing list: [`rb-general@lists.reproducible-builds.org`](https://lists.reproducible-builds.org/listinfo/rb-general)
