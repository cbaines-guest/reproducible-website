---
layout: report
year: "2022"
month: "05"
title: "Reproducible Builds in May 2022"
draft: false
date: 2022-06-06 12:23:26
---

[![]({{ "/images/reports/2022-05/reproducible-builds.png#right" | relative_url }})](https://reproducible-builds.org/)

Welcome to the May 2022 report from the [Reproducible Builds](https://reproducible-builds.org) project. In our reports we outline the most important things that we have been up to over the past month. As ever, if you are interested in contributing to the project, please visit our [*Contribute*]({{ "/contribute/" | relative_url }}) page on our website.

<br>

#### *Repfix* paper

[![]({{ "/images/reports/2022-05/repfix.png#right" | relative_url }})](http://oscar-lab.org/paper/icse_22_repfix.pdf)

Zhilei Ren, Shiwei Sun, Jifeng Xuan, Xiaochen Li, Zhide Zhou and He Jiang have published an academic paper titled *Automated Patching for Unreproducible Builds*:

> [..] fixing unreproducible build issues poses a set of challenges [..], among which we consider the localization granularity and the historical knowledge utilization as the most significant ones. To tackle these challenges, we propose a novel approach [called] RepFix that combines **tracing-based fine-grained localization with history-based patch generation** mechanisms.

The paper ([PDF, 3.5MB](http://oscar-lab.org/paper/icse_22_repfix.pdf)) uses the Debian `mylvmbackup` package as an example to show how *RepFix* can automatically generate patches to make software build reproducibly. As it happens, Reiner Herrmann submitted a patch for the `mylvmbackup` package which [has remained unapplied by the Debian package maintainer for over seven years](https://bugs.debian.org/782318), thus this paper inadvertently underscores that achieving reproducible builds will require both technical and social solutions.

#### Python variables

Johannes Schauer discovered a fascinating bug where simply naming your Python variable `_m` led to unreproducible `.pyc` files. In particular, the `types` module in Python 3.10 requires the following patch to make it reproducible:

```
--- a/Lib/types.py
+++ b/Lib/types.py
@@ -37,8 +37,8 @@ _ag = _ag()
 AsyncGeneratorType = type(_ag)
 
 class _C:
-    def _m(self): pass
-MethodType = type(_C()._m)
+    def _b(self): pass
+MethodType = type(_C()._b)
```

Simply renaming the dummy method from `_m` to `_b` was enough to workaround the problem. [Johannes' bug report](https://github.com/python/cpython/issues/92132) first led to a number of improvements in *diffoscope* to aid in dissecting `.pyc` files, but upstream identified this as caused by an issue surrounding [interned strings](https://en.wikipedia.org/wiki/String_interning) and is being tracked in [CPython bug #78274](https://github.com/python/cpython/issues/78274).

#### New SPDX team to incorporate build metadata in Software Bill of Materials

[SPDX](https://spdx.dev), the open standard for [Software Bill of Materials](https://en.wikipedia.org/wiki/Software_supply_chain) (SBOM), is continuously developed by a number of teams and committees. However, SPDX has welcomed a new addition; a team dedicated to enhancing metadata about software builds, complementing reproducible builds in creating a more secure software supply chain. The "SPDX Builds Team" has been working throughout May to define the universal primitives shared by all build systems, including the "who, what, where and how" of builds:

* Who: the identity of the person or organisation that controls the build infrastructure.

* What: the inputs and outputs of a given build, combining metadata about the build's configuration with an SBOM describing source code and dependencies.

* Where: the software packages making up the build system, from build orchestration tools such as [Woodpecker CI](https://woodpecker-ci.org/docs/intro) and [Tekton](https://tekton.dev/) to language-specific tools.

* How: the invocation of a build, linking metadata of a build to the identity of the person or automation tool that initiated it.

The SPDX Builds Team expects to have a usable data model by September, ready for inclusion in the SPDX 3.0 standard. The team welcomes new contributors, inviting those interested in joining to introduce themselves on the [SPDX-Tech](https://lists.spdx.org/g/spdx-tech) mailing list.

#### Talks at Debian Reunion Hamburg

Some of the Reproducible Builds team (Holger Levsen, Mattia Rizzolo, Roland Clobus, Philip Rinn, etc.) met in real life at the [Debian Reunion Hamburg](https://wiki.debian.org/DebianEvents/de/2022/DebianReunionHamburg/) ([official homepage](https://hamburg-2022.mini.debconf.org/)). There were several informal discussions amongst them, as well as two talks related to reproducible builds.

First, Holger Levsen gave a talk on the status of [*Reproducible Builds for bullseye and bookworm and beyond*](https://meetings-archive.debian.net/pub/debian-meetings/2022/Debian-Reunion-Hamburg/debian-reunion-hamburg-2022-22-reproduciblebuilds-for-bullseye-bookworm-and-beyond.webm) (WebM, 210MB):

[![]({{ "/images/reports/2022-05/holger-levsen-talk.png#center" | relative_url }})](https://meetings-archive.debian.net/pub/debian-meetings/2022/Debian-Reunion-Hamburg/debian-reunion-hamburg-2022-22-reproduciblebuilds-for-bullseye-bookworm-and-beyond.webm)

Secondly, Roland Clobus gave a talk called [*Reproducible builds as applied to non-compiler output*](https://meetings-archive.debian.net/pub/debian-meetings/2022/Debian-Reunion-Hamburg/debian-reunion-hamburg-2022-24-reproducible-builds-as-applied-to-non-compiler-output.webm) (WebM, 115MB):

[![]({{ "/images/reports/2022-05/roland-clobus-talk.png#center" | relative_url }})](https://gemmei.ftp.acc.umu.se/pub/debian-meetings/2022/Debian-Reunion-Hamburg/debian-reunion-hamburg-2022-24-reproducible-builds-as-applied-to-non-compiler-output.webm)

<br>


#### Supply-chain security attacks

This was another bumper month for supply-chain attacks in package repositories. Early in the month, Lance R. Vick noticed that the maintainer of the NPM `foreach` package let their personal email domain expire, so they bought it and now "[controls `foreach` on NPM and the 36,826 projects that depend on it](https://mastodon.social/@lrvick/108274265429826806)". Shortly afterwards, [Drew DeVault](https://drewdevault.com/) published a related blog post titled [*When will we learn?*](https://drewdevault.com/2022/05/12/Supply-chain-when-will-we-learn.html) that offers a brief timeline of major incidents in this area and, not uncontroversially, suggests that the "correct way to ship packages is with your distribution’s package manager".

#### Bootstrapping

"Bootstrapping" is a process for building software tools progressively from a primitive compiler tool and source language up to a full Linux development environment with GCC, etc. This is important given the amount of trust we put in existing compiler binaries. This month, a bootstrappable mini-kernel was announced. Called [boot2now](https://github.com/ironmeld/boot2now), it comprises a series of compilers in the form of bootable machine images.

#### Google's new Assured Open Source Software service

Google Cloud (the division responsible for the [Google Compute Engine](https://cloud.google.com/compute)) announced a new [Assured Open Source Software service](https://cloud.google.com/blog/products/identity-security/introducing-assured-open-source-software-service). Noting the considerable [650% year-over-year increase in cyberattacks aimed at open source suppliers](https://www.sonatype.com/hubfs/Q3%202021-State%20of%20the%20Software%20Supply%20Chain-Report/SSSC-Report-2021_0913_PM_2.pdf?hsLang=en-us), the new service claims to enable "enterprise and public sector users of open source software to easily incorporate the same OSS packages that Google uses into their own developer workflows". The announcement goes on to enumerate that packages curated by the new service would be:

* Regularly scanned, analyzed, and fuzz-tested for vulnerabilities.

* Have corresponding enriched metadata incorporating Container/Artifact Analysis data.

* Are built with Cloud Build including evidence of verifiable SLSA-compliance

* Are verifiably signed by Google.

* Are distributed from an Artifact Registry secured and protected by Google.

([Full announcement](https://cloud.google.com/blog/products/identity-security/introducing-assured-open-source-software-service))

#### A retrospective on the Rust programming language

[Andrew "bunnie" Huang](https://www.bunniestudios.com/) published a long blog post this month promising a "critical retrospective" on the Rust programming language. Amongst many acute observations about the evolution of the language's syntax (etc.), the post beings to critique the languages' approach to supply chain security ("Rust Has A Limited View of Supply Chain Security") and reproducibility ("You Can’t Reproduce Someone Else’s Rust Build"):

> There’s some bugs open with the Rust maintainers to address reproducible builds, but with the number of issues they have to deal with in the language, I am not optimistic that this problem will be resolved anytime soon. Assuming the only driver of the unreproducibility is the inclusion of OS paths in the binary, one fix to this would be to re-configure our build system to run in some sort of a chroot environment or a virtual machine that fixes the paths in a way that almost anyone else could reproduce. I say “almost anyone else” because this fix would be OS-dependent, so we’d be able to get reproducible builds under, for example, Linux, but it would not help Windows users where chroot environments are not a thing.

 ([Full post](https://www.bunniestudios.com/blog/?p=6375))

#### Reproducible Builds IRC meeting

The [minutes and logs from our May 2022 IRC meeting](http://meetbot.debian.net/reproducible-builds/2022/reproducible-builds.2022-05-31-14.59.html) have been published. In case you missed this one, our next IRC meeting will take place on **Tuesday 28th June at 15:00 UTC** on `#reproducible-builds` on the [OFTC](https://oftc.net) network.

#### A new tool to improve supply-chain security in Arch Linux

*kpcyrd* published yet another interesting tool related to reproducibility. Writing about the tool in a recent blog post, *kpcyrd* mentions that although many `PKGBUILDs` provide **authentication** in the context of signed Git tags (i.e. the ability to "verify the Git tag was signed by one of the two trusted keys"), they do not support **pinning**, ie. that "upstream could create a new signed Git tag with an identical name, and arbitrarily change the source code without the [maintainer] noticing". Conversely, other `PKGBUILD`s support pinning but not authentication. The new tool, [*auth-tarball-from-git*](https://github.com/kpcyrd/auth-tarball-from-git), fixes both problems, as [nearly outlined in *kpcyrd*'s original blog post](https://vulns.xyz/2022/05/auth-tarball-from-git/).

<br>

## [diffoscope](https://diffoscope.org)

[![]({{ "/images/reports/2022-04/diffoscope.svg#right" | relative_url }})](https://diffoscope.org)

[*diffoscope*](https://diffoscope.org) is our in-depth and content-aware diff utility. Not only can it locate and diagnose reproducibility issues, it can provide human-readable diffs from many kinds of binary formats. This month, Chris Lamb prepared and uploaded versions [`212`](https://diffoscope.org/news/diffoscope-212-released/), [`213`](https://diffoscope.org/news/diffoscope-213-released/) and [`214`](https://diffoscope.org/news/diffoscope-214-released/) to Debian *unstable*.

Chris also made the following changes:

* New features:

    * Add support for extracting `vmlinuz` Linux kernel images. [[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/707282bd)]
    * Support both `python-argcomplete` 1.x and 2.x. [[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/f837e7fb)]
    * Strip `sticky` etc. from `x.deb: sticky Debian binary package […]`. [[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/c6153ea8)]
    * Integrate test coverage with GitLab's concept of artifacts. [[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/7d050b44)][[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/0536d682)][[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/aa046f4e)]

* Bug fixes:

    * Don't mask differences in `.zip` or `.jar` central directory extra fields. [[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/e4ae650e)]
    * Don't show a binary comparison of `.zip` or `.jar` files if we have observed at least one nested difference. [[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/714d0b79)]

* Codebase improvements:

    * Substantially update comment for our calls to `zipinfo` and `zipinfo -v`. [[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/3f01ab7c)]
    * Use `assert_diff` in `test_zip` over calling `get_data` with a separate `assert`. [[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/decb8f9d)]
    * Don't call `re.compile` and then call `.sub` on the result; just call `re.sub` directly. [[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/5be08156)]
    * Clarify the comment around the difference between `--usage` and `--help`. [[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/85ef2872)]

* Testsuite improvements:

    * Test `--help` and `--usage`. [[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/60d7a058)]
    * Test that `--help` includes the file formats. [[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/fb191c85)]

Vagrant Cascadian added an external tool reference `xb-tool` for [GNU Guix](https://www.gnu.org/software/guix/) &nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/e6f7f3f9)] as well as updated the *diffoscope* package in GNU Guix itself&nbsp;[[...](https://git.savannah.gnu.org/cgit/guix.git/commit/?id=ce1c88a29b56ac7abe8413997464508b46d4f970)][[...](https://git.savannah.gnu.org/cgit/guix.git/commit/?id=de68877a517ba058f67086d444d7265c9c803679)][[...](https://git.savannah.gnu.org/cgit/guix.git/commit/?id=83f444de56957ff6a6fa76605c73a57ea76c6e3c)].


<br>

## Distribution work

[![]({{ "/images/reports/2022-05/debian.png#right" | relative_url }})](https://debian.org/)

In Debian, 41 reviews of Debian packages were added, 85 were updated and 13 were removed this month adding to [our knowledge about identified issues](https://tests.reproducible-builds.org/debian/index_issues.html). A number of issue types have been updated, including adding a new `nondeterministic_ordering_in_deprecated_items_collected_by_doxygen` toolchain issue [[...](https://salsa.debian.org/reproducible-builds/reproducible-notes/commit/6a9f92f3)] as well as ones for `mono_mastersummary_xml_files_inherit_filesystem_ordering` [[...](https://salsa.debian.org/reproducible-builds/reproducible-notes/commit/46ed277c)], `extended_attributes_in_jar_file_created_without_manifest` [[...](https://salsa.debian.org/reproducible-builds/reproducible-notes/commit/672cdb0a)] and `apxs_captures_build_path` [[...](https://salsa.debian.org/reproducible-builds/reproducible-notes/commit/bc820fbd)].

[![]({{ "/images/reports/2022-05/opensuse.png#right" | relative_url }})](https://www.opensuse.org/)

Vagrant Cascadian performed a rough check of the [reproducibility of core package sets](https://lists.reproducible-builds.org/pipermail/rb-general/2022-May/002557.html) in [GNU Guix](https://www.gnu.org/software/guix/), and in [openSUSE](https://www.opensuse.org/), Bernhard M. Wiedemann posted his [usual monthly reproducible builds status report](https://lists.opensuse.org/archives/list/factory@lists.opensuse.org/thread/IXE24I26YOYRKBHV6BOESCG6UNRFTVGL/).

<br>

## Upstream patches

The Reproducible Builds project detects, dissects and attempts to fix as many currently-unreproducible packages as possible. We endeavour to send all of our patches upstream where appropriate. This month, we wrote a large number of such patches, including:

* Bernhard M. Wiedemann:

    * [`gtkmm-documentation`](https://gitlab.gnome.org/GNOME/gtkmm-documentation/-/merge_requests/15) (merged; sorting issue)
    * [`librespot`](https://github.com/librespot-org/librespot/pull/995) (merged; random `BuildID` issue)
    * [`lirc`](https://sourceforge.net/p/lirc/git/merge-requests/36/) (merged)
    * [`lsof`](https://bugzilla.opensuse.org/show_bug.cgi?id=1199709) (`uname`/`hostname` problem)
    * [`solanum`](https://github.com/solanum-ircd/solanum/pull/337) (merged, possibly a race condition)

* Chris Lamb:

    * [#1010845](https://bugs.debian.org/1010845) filed against [`logapp`](https://tracker.debian.org/pkg/logapp).
    * [#1010854](https://bugs.debian.org/1010854) filed against [`mono`](https://tracker.debian.org/pkg/mono).
    * [#1010855](https://bugs.debian.org/1010855) filed against [`longrun`](https://tracker.debian.org/pkg/longrun).
    * [#1011452](https://bugs.debian.org/1011452) filed against [`rust-simplelog`](https://tracker.debian.org/pkg/rust-simplelog).
    * [#1011752](https://bugs.debian.org/1011752) filed against [`freesas`](https://tracker.debian.org/pkg/freesas).
    * [`PeachPy`](https://github.com/Maratyszcza/PeachPy/pull/108#pullrequestreview-981950994) (merged, sort entries)

* Johannes Schauer Marin Rodrigues:

    * [#1010957](https://bugs.debian.org/1010957) filed against [`man-db`](https://tracker.debian.org/pkg/man-db).

* Vagrant Cascadian:

    * [#1010462](https://bugs.debian.org/1010462) filed against [`mtink`](https://tracker.debian.org/pkg/mtink).
    * [#1010463](https://bugs.debian.org/1010463) filed against [`fceux`](https://tracker.debian.org/pkg/fceux).
    * [#1010466](https://bugs.debian.org/1010466) filed against [`glob2`](https://tracker.debian.org/pkg/glob2).
    * [#1010483](https://bugs.debian.org/1010483) filed against [`coinor-cgl`](https://tracker.debian.org/pkg/coinor-cgl).
    * [#1010486](https://bugs.debian.org/1010486) filed against [`metapixel`](https://tracker.debian.org/pkg/metapixel).
    * [#1010781](https://bugs.debian.org/1010781) filed against [`ragel`](https://tracker.debian.org/pkg/ragel).
    * [#1010785](https://bugs.debian.org/1010785) filed against [`gdome2`](https://tracker.debian.org/pkg/gdome2).
    * [#1010787](https://bugs.debian.org/1010787) filed against [`sgml-base-doc`](https://tracker.debian.org/pkg/sgml-base-doc).
    * [#1010789](https://bugs.debian.org/1010789) filed against [`xarclock`](https://tracker.debian.org/pkg/xarclock).
    * [#1010790](https://bugs.debian.org/1010790) filed against [`xgammon`](https://tracker.debian.org/pkg/xgammon).
    * [#1010825](https://bugs.debian.org/1010825) filed against [`lwatch`](https://tracker.debian.org/pkg/lwatch).
    * [#1010828](https://bugs.debian.org/1010828) filed against [`bbrun`](https://tracker.debian.org/pkg/bbrun).
    * [#1010830](https://bugs.debian.org/1010830) filed against [`gscanbus`](https://tracker.debian.org/pkg/gscanbus).
    * [#1010859](https://bugs.debian.org/1010859) filed against [`libnss-gw-name`](https://tracker.debian.org/pkg/libnss-gw-name).
    * [#1010870](https://bugs.debian.org/1010870) filed against [`pidgin-blinklight`](https://tracker.debian.org/pkg/pidgin-blinklight).
    * [#1010871](https://bugs.debian.org/1010871) filed against [`dvbtune`](https://tracker.debian.org/pkg/dvbtune).
    * [#1010872](https://bugs.debian.org/1010872) filed against [`efax`](https://tracker.debian.org/pkg/efax).
    * [#1010944](https://bugs.debian.org/1010944) filed against [`quelcom`](https://tracker.debian.org/pkg/quelcom).
    * [#1010948](https://bugs.debian.org/1010948) filed against [`xine-lib-1.2`](https://tracker.debian.org/pkg/xine-lib-1.2).
    * [#1011034](https://bugs.debian.org/1011034) filed against [`fusesmb`](https://tracker.debian.org/pkg/fusesmb).
    * [#1011036](https://bugs.debian.org/1011036) filed against [`mailfront`](https://tracker.debian.org/pkg/mailfront).
    * [#1011104](https://bugs.debian.org/1011104) filed against [`convlit`](https://tracker.debian.org/pkg/convlit).
    * [#1011109](https://bugs.debian.org/1011109) filed against [`bitstormlite`](https://tracker.debian.org/pkg/bitstormlite).
    * [#1011257](https://bugs.debian.org/1011257) filed against [`coinor-osi`](https://tracker.debian.org/pkg/coinor-osi).
    * [#1011402](https://bugs.debian.org/1011402) filed against [`razor`](https://tracker.debian.org/pkg/razor).
    * [#1011405](https://bugs.debian.org/1011405) filed against [`autoclass`](https://tracker.debian.org/pkg/autoclass).
    * [#1011428](https://bugs.debian.org/1011428) filed against [`cdbackup`](https://tracker.debian.org/pkg/cdbackup).
    * [#1011429](https://bugs.debian.org/1011429) filed against [`dds2tar`](https://tracker.debian.org/pkg/dds2tar).
    * [#1011469](https://bugs.debian.org/1011469) filed against [`transcalc`](https://tracker.debian.org/pkg/transcalc).
    * [#1011470](https://bugs.debian.org/1011470) filed against [`libapache2-mod-authz-unixgroup`](https://tracker.debian.org/pkg/libapache2-mod-authz-unixgroup).
    * [#1011471](https://bugs.debian.org/1011471) filed against [`mgdiff`](https://tracker.debian.org/pkg/mgdiff).
    * [#1011478](https://bugs.debian.org/1011478) filed against [`scsitools`](https://tracker.debian.org/pkg/scsitools).
    * [#1011479](https://bugs.debian.org/1011479) filed against [`fstrcmp`](https://tracker.debian.org/pkg/fstrcmp).
    * [#1011480](https://bugs.debian.org/1011480) filed against [`libxsettings-client`](https://tracker.debian.org/pkg/libxsettings-client).
    * [#1011481](https://bugs.debian.org/1011481) filed against [`tamil-gtk2im`](https://tracker.debian.org/pkg/tamil-gtk2im).
    * [#1011486](https://bugs.debian.org/1011486) filed against [`tdfsb`](https://tracker.debian.org/pkg/tdfsb).
    * [#1011488](https://bugs.debian.org/1011488) filed against [`stymulator`](https://tracker.debian.org/pkg/stymulator).
    * [#1011489](https://bugs.debian.org/1011489) filed against [`wiipdf`](https://tracker.debian.org/pkg/wiipdf).
    * [#1011490](https://bugs.debian.org/1011490) filed against [`gdigi`](https://tracker.debian.org/pkg/gdigi).
    * [#1011491](https://bugs.debian.org/1011491) filed against [`getstream`](https://tracker.debian.org/pkg/getstream).
    * [#1011493](https://bugs.debian.org/1011493) filed against [`freecdb`](https://tracker.debian.org/pkg/freecdb).
    * [#1011495](https://bugs.debian.org/1011495) filed against [`modglue`](https://tracker.debian.org/pkg/modglue).
    * [#1011496](https://bugs.debian.org/1011496) filed against [`nwall`](https://tracker.debian.org/pkg/nwall).
    * [#1011498](https://bugs.debian.org/1011498) filed against [`parprouted`](https://tracker.debian.org/pkg/parprouted).
    * [#1011499](https://bugs.debian.org/1011499) filed against [`imagination`](https://tracker.debian.org/pkg/imagination).
    * [#1011500](https://bugs.debian.org/1011500) filed against [`tuxcmd-modules`](https://tracker.debian.org/pkg/tuxcmd-modules).
    * [#1011501](https://bugs.debian.org/1011501) filed against [`libapache2-mod-authn-yubikey`](https://tracker.debian.org/pkg/libapache2-mod-authn-yubikey).
    * [#1011503](https://bugs.debian.org/1011503) filed against [`libapache2-mod-auth-plain`](https://tracker.debian.org/pkg/libapache2-mod-auth-plain).
    * [lcsync](https://github.com/librestack/lcsync/pull/1) (remove build paths)

<br>

## Reproducible builds website

Chris Lamb updated the [main Reproducible Builds website and documentation](https://reproducible-builds.org/) in a number of small ways, but also prepared and published an interview with Jan Nieuwenhuizen about [Bootstrappable Builds, GNU Mes and GNU Guix](https://reproducible-builds.org/news/2022/05/18/jan-nieuwenhuizen-on-bootrappable-builds-gnu-mes-and-gnu-guix/). [[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/81f1a171)][[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/22f590d7)][[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/a2328750)][[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/abf968da)]

In addition, Tim Jones added a link to the [Talos Linux](https://www.talos.dev/) project&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/a2a775ba)] and *billchenchina* fixed a dead link&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/90f86dfa)].

<br>

## Testing framework

[![]({{ "/images/reports/2022-05/testframework.png#right" | relative_url }})](https://tests.reproducible-builds.org/)

The Reproducible Builds project runs a significant testing framework at [tests.reproducible-builds.org](https://tests.reproducible-builds.org), to check packages and other artifacts for reproducibility. This month, the following changes were made:

* Holger Levsen:

    * Add support for detecting running kernels that require attention.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/84f23b6d)]
    * Temporarily configure a host to support performing Debian builds for packages that lack `.buildinfo` files.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/a29ebb81)]
    * Update generated webpages to clarify wishes for feedback.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/b90852a8)]
    * Update copyright years on various scripts.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/578a3ef1)]

* Mattia Rizzolo:

    * Provide a facility so that Debian Live image generation can copy a file remotely.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/7e5d425c)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/335d93b5)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/8f8182f1)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/e27cd68f)]

* Roland Clobus:

    * Add initial support for testing generated images with [OpenQA](http://open.qa/).&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/535490bb)]

And finally, as usual, node maintenance was also performed by Holger Levsen [[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/9f9442d8)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/7ea26a05)].

<br>

## Misc news

On [our mailing list](https://lists.reproducible-builds.org/listinfo/rb-general/) this month:

* John Neffenger posted that the early-access release of [OpenJDK version 19 build 21 is reproducible](https://lists.reproducible-builds.org/pipermail/rb-general/2022-May/002565.html).

* Mattia Rizzolo added a request around [tentatively planning a Reproducible Builds summit in 2022](https://lists.reproducible-builds.org/pipermail/rb-general/2022-May/002582.html).

* Bernhard M. Wiedemann [posted about a Reproducible Builds meetup](https://lists.reproducible-builds.org/pipermail/rb-general/2022-May/002584.html) at the openSUSE conference in Nuremberg.

* Luca Boccassi asked for [help getting `arm64` binaries to build reproducibly](https://lists.reproducible-builds.org/pipermail/rb-general/2022-May/002586.html) for the Debian [`systemd`](https://www.freedesktop.org/wiki/Software/systemd/) package.

* Yaobin Wen asked a number of questions in an attempt to discover the [best practices for manage Debian `.dsc` files](https://lists.reproducible-builds.org/pipermail/rb-general/2022-May/002590.html) using [*reprepro*](https://manpages.debian.org/unstable/reprepro/reprepro.1.en.html).


<br>

## Contact

If you are interested in contributing to the Reproducible Builds project, please visit our [*Contribute*](https://reproducible-builds.org/contribute/) page on our website. However, you can get in touch with us via:

 * IRC: `#reproducible-builds` on `irc.oftc.net`.

 * Twitter: [@ReproBuilds](https://twitter.com/ReproBuilds)

 * Mailing list: [`rb-general@lists.reproducible-builds.org`](https://lists.reproducible-builds.org/listinfo/rb-general)
