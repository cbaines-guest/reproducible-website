---
layout: default
title: Contribute to Arch Linux
permalink: /contribute/archlinux/
---

## Task suggestions for Arch Linux

1. Join #archlinux-reproducible on <em>irc.libera.chat</em>.

1. Check the unreproducible packages on your system with `arch-repro-status -f BAD`.
   Additionally adding `-i` to that command allows you to interactively browse the build
   logs and diffoscope output per package.

1. If you maintain packages in Arch Linux the [Developer Dashboard](https://archlinux.org/devel/)
   has a report with your unreproducible packages.

1. Report packaging issues which cause unreproducible packages on the
   [bugtracker](https://bugs.archlinux.org).

## External links

* [Arch Linux reproducible builds status](https://reproducible.archlinux.org/)
* [arch-repro-status](https://gitlab.archlinux.org/archlinux/arch-repro-status)
